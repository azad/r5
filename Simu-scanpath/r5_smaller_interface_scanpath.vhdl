LIBRARY ieee;
use ieee.numeric_std.all;
USE ieee.std_logic_1164.all;

library work;
use work.all;
use work.ram.all;

library work;
use work.out_value.all;

use STD.textio.all;
use work.std_logic_textio.all;

entity r5_smaller_interface_scanpath is
end r5_smaller_interface_scanpath;


architecture r5_smaller_interface_scanpath of r5_smaller_interface_scanpath is
  function to_bstring(sl : std_logic) return string is
    variable sl_str_v : string(1 to 3);  -- std_logic image with quotes around
    begin
      sl_str_v := std_logic'image(sl);
      return "" & sl_str_v(2);  -- "" & character to get string
    end function;

  function to_bstring(slv : std_logic_vector) return string is
      alias    slv_norm : std_logic_vector(1 to slv'length) is slv;
      variable sl_str_v : string(1 to 1);  -- String of std_logic
      variable res_v    : string(1 to slv'length);
    begin
      for idx in slv_norm'range loop
        sl_str_v := to_bstring(slv_norm(idx));
        res_v(idx) := sl_str_v(1);
      end loop;
      return res_v;
  end function;

  -- We use dcache as a generic memory interface
	component dcache
  generic(
--    latency : natural
    write_latency : std_logic_vector(3 downto 0);
    read_latency : std_logic_vector(3 downto 0);
    random_latency : std_logic
  );
	port(
	-- Dcache outterface
			mem_adr			: in Std_Logic_Vector(31 downto 0);
			mem_stw			: in Std_Logic;
			mem_sth			: in Std_Logic;
			mem_stb			: in Std_Logic;
			mem_load			: in Std_Logic;

			mem_data			: in Std_Logic_Vector(31 downto 0);
			dc_data			: out Std_Logic_Vector(31 downto 0);
			dc_stall			: out Std_Logic;

			ck					: in Std_logic);
	end component;

  component r5_smaller_interface_scan
  port ( bits_valid : in bit
       ; ck         : in bit
       ; reset_n    : in bit
       ; scin       : in bit
       ; test       : in bit
       ; bits_in    : in  bit_vector(31 downto 0)
       ; req        : out bit
       ; bits_enable : inout bit
       ; scout      : inout   bit
       ; format_rw  : inout bit_vector(1 downto 0)
       ; bits_out   : inout  bit_vector(31 downto 0)
       ; vdd        : in bit
       ; vss        : in bit
       );
	end component;


  signal bits : bit_vector(31 downto 0);

	signal	mem_adr			: bit_Vector(31 downto 0);
	signal	mem_stw			: Std_Logic;
	signal	mem_sth			: Std_Logic;
	signal	mem_stb			: Std_Logic;
	signal	mem_load			: Std_Logic;

	signal	mem_data			: bit_Vector(31 downto 0);
	signal	dc_data			: Std_Logic_Vector(31 downto 0);
	signal	dc_stall			: Std_Logic;

	signal	ck					: bit;
	signal	reset_n			: bit;
	signal	vdd				: bit := '1';
	signal	vss				: bit := '0';

	signal	GoodAdr			: Std_Logic_Vector(31 downto 0) ;
	signal	BadAdr			: Std_Logic_Vector(31 downto 0) ;


  signal req : bit;
  signal bits_in, bits_out : bit_vector(31 downto 0);
  signal bits_enable : bit;
  signal bits_valid : bit;
  signal format_rw : bit_vector(1 downto 0);

  signal scin : bit ;
  signal test : bit ;
  signal scout : bit ;

  constant write_latency : std_logic_vector(3 downto 0) := X"0"; -- + stall for x cycle
  constant read_latency : std_logic_vector(3 downto 0) := X"0"; -- + stall for x cycle
  constant do_random : std_logic := '0';
--  constant scan_path_size : natural := 413;
  constant scan_path_size : natural := 518;
--  constant scan_path_size : natural := 500;

  file file_INPUT : text;
  file file_OUTPUT : text;

  type state is (IDLE,WRITE, WRITE_OP, READ, READ_OP, OK);
  signal cur_state, next_state : state;

	begin
	--  Component instantiation.


	cache_i : dcache
  generic map(
    write_latency =>  write_latency,
    read_latency => read_latency,
    random_latency => do_random
  )
	port map (	mem_adr	=> to_stdlogicvector(mem_adr),
					mem_stw	=> mem_stw,
					mem_sth	=> mem_sth,
					mem_stb	=> mem_stb,
					mem_load	=> mem_load,

					mem_data	=> to_stdlogicvector(mem_data),
					dc_data	=> dc_data,
					dc_stall	=> dc_stall,
					ck			=> to_stdulogic(ck)
      );


  core_smaller : r5_smaller_interface_scan
  port map(
      bits_valid => bits_valid,
      ck  => ck,
      reset_n => reset_n,
      scin => scin,
      test => test,
      bits_in => bits_in,
      req => req,
      bits_enable => bits_enable,
      scout => scout,
      format_rw => format_rw,
      bits_out => bits_out,
      VDD => vdd,
      VSS => vss
    );

-- Emulate tristate
bits <= bits_out when bits_enable = '1' else bits_in;

process (ck)
begin
--if rising_edge(ck) then -- No rising edge for bit before vhdl 2008
if ck'event and ck = '1' then
	if (reset_n = '0') then
    cur_state <= IDLE;
	else
    cur_state <= next_state;
    case next_state is
      when IDLE =>
        mem_adr <= mem_adr;
        mem_stw <= '0';
        mem_sth <= '0';
        mem_stb <= '0';
        mem_data <= mem_data;
        mem_load <= '0';
--        bits <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
        bits_valid <= '0';
      when WRITE =>
        mem_adr <= bits;
        mem_stw <= '0';
        mem_sth <= '0';
        mem_stb <= '0';
        mem_data <= mem_data;
        mem_load <= '0';
--        bits <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
        bits_valid <= '0';
      when WRITE_OP =>
        mem_adr <= mem_adr;
        if format_rw = "01" then
          mem_stw <= '1';
        else
          mem_stw <= '0';
        end if;
        if format_rw = "10" then
          mem_sth <= '1';
        else
          mem_sth <= '0';
        end if;
        if format_rw = "11" then
          mem_stb <= '1';
        else
          mem_stb <= '0';
        end if;
        mem_data <= bits;
        mem_load <= '0';
--        bits <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
        bits_valid <= '0';
      when READ =>
        mem_adr <= bits;
        mem_stw <= '0';
        mem_sth <= '0';
        mem_stb <= '0';
        mem_data <= mem_data;
        mem_load <= '1';
--        bits <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
        bits_valid <= '0';
      when READ_OP =>
        mem_adr <= mem_adr;
        mem_stw <= '0';
        mem_sth <= '0';
        mem_stb <= '0';
        mem_data <= mem_data;
        mem_load <= '0';
        bits_in <= to_bitvector(dc_data);
        bits_valid <= '0';
      when OK =>
        mem_adr <= mem_adr;
        mem_stw <= '0';
        mem_sth <= '0';
        mem_stb <= '0';
        mem_data <= mem_data;
        mem_load <= '0';
        bits_in <= bits_in;
        bits_valid <= '1';
    end case;
	end if;
end if;
end process;

process (cur_state, req, format_rw, dc_stall)
begin
	case cur_state is
  when IDLE =>
    if req = '1' then
      if format_rw = "00" then
        next_state <= READ;
      else
        next_state <= WRITE;
      end if;
    else
      next_state <= IDLE;
    end if;
  when WRITE =>
    next_state <= WRITE_OP;
  when WRITE_OP =>
    if dc_stall = '0' then
      next_state <= OK;
    else
      next_state <= cur_state;
    end if;
  when READ =>
    if dc_stall = '0' then
      next_state <= READ_OP;
    else
      next_state <= cur_state;
    end if;
  when READ_OP => -- TODO maybe this state can be remove READ_OP
    next_state <= OK;
  when OK =>
    next_state <= IDLE;
  end case;
end process;

    file_open(file_INPUT, "scanpath.in",  read_mode);
    file_open(file_OUTPUT, "scanpath.out",  write_mode);

process
  variable iadr : signed(31 downto 0);
  variable v_ILINE     : line;
  variable v_OLINE     : line;
  variable scan_in : bit_vector(scan_path_size downto 0);
  variable nb_cycle : integer;
  variable scan_out_expected : bit_vector(scan_path_size downto 0);
  variable scan_output : bit_vector(scan_path_size downto 0);
begin
  GoodAdr <= std_logic_vector(TO_SIGNED(mem_goodadr, 32));
	BadAdr <= std_logic_vector(TO_SIGNED(mem_badadr, 32));

	reset_n <= '0';
	ck <= '0';
	wait for 100 ns;
	ck <= '1';
	wait for 100 ns;
	reset_n <= '1';

  readline(file_INPUT, v_ILINE);
  read(v_ILINE, scan_in);

  -- Start to insert scan value
  test <= '1';
  for i in scan_path_size downto 0 loop
    report "Insert" &  integer'image(i) & " => " & bit'image(scan_in(i)) severity note;
    scin <= scan_in(i);
		ck <= '0';
		wait for 100 ns;
		ck <= '1';
		wait for 100 ns;
  end loop;
  test <= '0';
  readline(file_INPUT, v_ILINE);
  read(v_ILINE, nb_cycle);

  report "Value insered, start test";
	while not (nb_cycle = 0) loop
		ck <= '0';
		wait for 100 ns;
		ck <= '1';
		wait for 100 ns;
    nb_cycle := nb_cycle - 1;
	end loop;
  report "end test";

  test <= '1';

  for i in scan_path_size downto 0 loop
    scan_output(i) := scout; -- first out is last expected
		ck <= '0';
		wait for 100 ns;
		ck <= '1';
		wait for 100 ns;
  end loop;

  readline(file_INPUT, v_ILINE);
  read(v_ILINE, scan_out_expected);

  for i in 0 to scan_path_size loop
    if scan_out_expected(i) /= scan_output(i) then
      report "FAIL : " & integer'image(i) & " bit (" & bit'image(scan_output(i)) & ") is not same than expected (" & bit'image(scan_out_expected(i)) & ")";
    end if;
  end loop;

	iadr := signed(to_stdulogicvector(mem_adr));
  if mem_adr = to_bitvector(GoodAdr) then
    report "GOOD!!!" severity note;
    good_end(TO_INTEGER(iadr));
    --good_end();
  end if;

  if mem_adr = to_bitvector(BadAdr) then
    report "BAD!!!" severity note;
    bad_end(TO_INTEGER(iadr));
    --bad_end();
  end if;

	assert false report "end of test" severity note;

wait;
end process;

end r5_smaller_interface_scanpath;
