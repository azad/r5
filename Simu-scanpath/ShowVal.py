#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os




def loadScanPathConfiguration ( filePath ):
    sys.path.append( os.path.split(filePath)[0] )


    confFile = filePath.replace(".py", "")
    if not os.path.isfile(confFile+'.py'):
      sys.exit( 1, 'ScanPath configuration file <%s.py> is missing.' % confFile )
    
    confModule = __import__( os.path.split(confFile)[1], globals(), locals(), os.path.split(confFile)[1] )

    if not confModule.__dict__.has_key('scanpath'):
      sys.exit( 1, 'Module <%s> do not provides the scanpath variable, skipped.' \
                       % confFile )

    return confModule.__dict__['scanpath']


if len(sys.argv) != 4:
    print("Usage : ./CmpOutput.py [scanpath config file json] [scanpath.in file] [scanpath.out file]")
    sys.exit()

print("Load scanpath config file %s" % sys.argv[1])
scanpath_dict = loadScanPathConfiguration(sys.argv[1])

scanpath = []

for instance in scanpath_dict:
    instance_name=list(instance.keys())[0]
    for signal in instance[instance_name]:
        if type(signal) == type(""):
            scanpath.append(instance_name + "/" + signal)
        else: # it's a dict, so sub instance
            instance_bis_name=list(signal.keys())[0]
            for signal_bis in signal[instance_bis_name]:
                scanpath.append(instance_name + "/" + instance_bis_name + "/" + signal_bis)

f = open(sys.argv[2], "r")
input_values = f.readline().replace("\n","") # Remove \n and reverse string to index with bit number
#input_values=input_values[::-1]
nb_cycles = f.readline()
expected_output = f.readline().replace("\n","") # Remove \n and reverse string to index with bit number
#expected_output=expected_output[::-1]
f.close()


f = open(sys.argv[3], "r")
output_val = f.readline().replace("\n","")
#output_val=output_val[::-1]
f.close()

scanpath_size = len(scanpath)
i=0
print("Inseré |Attendu | Lu | Signal  (bit nb)")
for i in range(0, scanpath_size):
    print(input_values[i] + "      |" + expected_output[i] + "       |" + output_val[i] + "   |" + scanpath[i] + " (" + str(i) + ")")
    i=i+1
