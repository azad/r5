if [ $# -ne 3 ]; then
  echo "Usage : bash make_scanpath_input_vector.sh binary dir_for_vector scanpath_config.path"
  exit 1
fi

riscvbinary=$1
vectordir=$2
scanpathconfig=$3

simupostsynth=../Simu-post-synthesis/riscv_softsimu_post_synthesis
readsignal=../misc/read_signal/read_signal

if [ ! -f "$readsignal" ]; then
  echo "Please make "read_signal" in misc/read_signal"
  exit 1
fi

if [ ! -f "$simupostsynth" ]; then
  echo "Please make post synthesis ${simupostsynth}"
  exit 1
fi

if [ -f "$vectordir" ] || [ -d "$vectordir" ]; then
  echo "$vectordir already exist"
  exit 1
fi
mkdir -p ${vectordir}

if [ ! -f "$riscvbinary" ]; then
  echo "Riscv binary $riscvbinary does not exist"
  exit 1
fi

if [ ! -f "$scanpathconfig" ]; then
  echo "Configuration .path $scanpathconfig does not exist"
  exit 1
fi


cp $scanpathconfig ${vectordir}/signals.path
sed -i 's#^#/riscv_softsimu_post_synthesis/core_i#'  ${vectordir}/signals.path
${simupostsynth} ${riscvbinary} --wave=${vectordir}/signal.ghw
${readsignal} ${vectordir}/signal.ghw  ${vectordir}/signals.path  ${vectordir}


for i in `ls -tr ${vectordir}/*.in`
do
  head -n 3 $i >> ${vectordir}/scanpath_all.in
done



echo "That's all done ! Copy one file from ${vectordir} to your local dir with name 'scanpath.in'. And launch simu with same binary : ./riscv_softsimu_scanpath ${riscvbinary}"
