LIBRARY ieee;
use ieee.numeric_std.all;
USE ieee.std_logic_1164.all;

library work;
use work.all;
use work.ram.all;

library work;
use work.out_value.all;

use STD.textio.all;
use work.std_logic_textio.all;

entity riscV_SoftSIMU_scanpath is
end riscV_SoftSIMU_scanpath;


architecture riscV_SoftSIMU_scanpath of riscV_SoftSIMU_scanpath is
  function to_bstring(sl : std_logic) return string is
    variable sl_str_v : string(1 to 3);  -- std_logic image with quotes around
    begin
      sl_str_v := std_logic'image(sl);
      return "" & sl_str_v(2);  -- "" & character to get string
    end function;

  function to_bstring(slv : std_logic_vector) return string is
      alias    slv_norm : std_logic_vector(1 to slv'length) is slv;
      variable sl_str_v : string(1 to 1);  -- String of std_logic
      variable res_v    : string(1 to slv'length);
    begin
      for idx in slv_norm'range loop
        sl_str_v := to_bstring(slv_norm(idx));
        res_v(idx) := sl_str_v(1);
      end loop;
      return res_v;
  end function;

	component icache
  generic(
    latency : natural;
    random_latency : std_logic -- latency will be random beetween 1 and latency
  );
	port(
      ck : std_logic;
	-- Icache interface
			if_adr			: in std_logic_vector(31 downto 0) ;
			if_adr_valid	: in std_logic;

			ic_inst			: out std_logic_vector(31 downto 0) ;
			ic_stall			: out std_logic);
	end component;

	component dcache
  generic(
--    latency : natural
    write_latency : std_logic_vector(3 downto 0);
    read_latency : std_logic_vector(3 downto 0);
    random_latency : std_logic
  );
	port(
	-- Dcache outterface
			mem_adr			: in std_logic_Vector(31 downto 0);
			mem_stw			: in std_logic;
			mem_sth			: in std_logic;
			mem_stb			: in std_logic;
			mem_load			: in std_logic;

			mem_data			: in std_logic_Vector(31 downto 0);
			dc_data			: out std_logic_Vector(31 downto 0);
			dc_stall			: out std_logic;

			ck					: in std_logic);
	end component;
  component riscV_Core_scan
  port( -- Global Interface
      ck     : in bit;                               -- Core Clock
      reset_n   : in bit;                               -- Global Asynchrone Reset
      VDD     : in bit;                                     -- Power Supply VDD Voltage
      VSS     : in bit;                                     -- Power Supply VSS Voltage

      -- Scan path
      scin    : in      bit;
      test    : in      bit;
      scout   : inout   bit;

      -- Instruction Cache Interface
      inst_adr    : inout bit_vector(31 downto 0);      -- Instruction address
      inst_req    : inout bit;                          -- Instruction request
      inst_in     : in  bit_vector(31 downto 0);      -- Instruction given by ICache
      inst_valid  : in  bit;                          -- Instruction valide

      -- Data Cache Interface
      data_adr    : inout bit_vector(31 downto 0);      -- Data address
      data_load_w : inout bit;
      data_store_b : inout bit;
      data_store_h : inout bit;
      data_store_w : inout bit;
      data_out    : inout bit_vector(31 downto 0);      -- Data out (Store)
      data_in     : in  bit_vector(31 downto 0);      -- Data given by DCache
      data_valid  : in  bit                           -- Data valide

      -- Extensions (in option, not supported here)
      --core_mode   : out bit_vector(1 downto 0);       -- Processor Mode (Kernel, User, etc..)
      --irq_in      : in  bit;                          -- Signal IRQ in
      --except_in   : in  bit;                          -- For exceptions (bad address, bad mode, etc...)
    );
	end component;
	signal	if_adr			: bit_Vector(31 downto 0) ;
	signal	if_adr_valid	: bit;

	signal	ic_inst			: bit_Vector(31 downto 0) ;
	signal	ic_stall			: bit;
	signal	ic_inst_sl			: std_logic_Vector(31 downto 0) ;
	signal	ic_stall_sl			: std_logic;
   
	signal	mem_adr			: bit_Vector(31 downto 0);
	signal	mem_stw			: bit;
	signal	mem_sth			: bit;
	signal	mem_stb			: bit;
	signal	mem_load			: bit;

	signal	mem_data			: bit_Vector(31 downto 0);
	signal	dc_data			: bit_Vector(31 downto 0);
	signal	dc_stall			: bit;
	signal	dc_data_sl			: std_logic_Vector(31 downto 0) ;
	signal	dc_stall_sl			: std_logic;

	signal	ck					: bit;
	signal	reset_n			: bit;
	signal	vdd				: bit := '1';
	signal	vss				: bit := '0';

	signal	GoodAdr			: std_logic_Vector(31 downto 0) ;
	signal	BadAdr			: std_logic_Vector(31 downto 0) ;

  signal dvalid : bit;
  signal ivalid : bit;
  signal mem_size : bit_vector( 2 downto 0);
  signal mem_req : bit;
  signal mem_w_not_r : bit;

  signal scin : bit ;
  signal test : bit ; -- do use test
  signal scout : bit ;

  signal	if_adr_valid_not_test	: bit;
	signal	mem_stw_not_test			: bit;
	signal	mem_sth_not_test			: bit;
	signal	mem_stb_not_test			: bit;
	signal	mem_load_not_test			: bit;

  constant latency_icache : natural := 0;
  constant write_latency : std_logic_vector(3 downto 0) := X"0"; -- + stall for x cycle
  constant read_latency : std_logic_vector(3 downto 0) := X"0"; -- + stall for x cycle
  constant do_random : std_logic := '0';
--  constant scan_path_size : natural := 413;
  constant scan_path_size : natural := 417;

  file file_INPUT : text;
  file file_OUTPUT : text;

	begin
	--  Component instantiation.

	icache_i : icache
  generic map(
    latency => latency_icache,
    random_latency => do_random
  )
	port map (
          ck => to_stdulogic(ck),
          if_adr			=> to_stdlogicvector(if_adr),
					if_adr_valid	=> to_stdulogic(if_adr_valid_not_test),
					ic_inst			=> ic_inst_sl,
					ic_stall			=> ic_stall_sl);
	ic_inst <= to_bitvector(ic_inst_sl);
	ic_stall <= to_bit(ic_stall_sl);

	dcache_i : dcache
  generic map(
    write_latency =>  write_latency,
    read_latency => read_latency,
    random_latency => do_random
  )
	port map (	mem_adr	=> to_stdlogicvector(mem_adr),
					mem_stw	=> to_stdulogic(mem_stw_not_test),
					mem_sth	=> to_stdulogic(mem_sth_not_test),
					mem_stb	=> to_stdulogic(mem_stb_not_test),
					mem_load	=> to_stdulogic(mem_load_not_test),

					mem_data	=> to_stdlogicvector(mem_data),
					dc_data	=> dc_data_sl,
					dc_stall	=> dc_stall_sl,
					ck			=> to_stdulogic(ck));
	dc_data <= to_bitvector(dc_data_sl);
	dc_stall <= to_bit(dc_stall_sl);
  
  core_i : riscV_Core_scan
  port map( -- Global Interface
      ck  => ck,
      reset_n => reset_n,
      VDD => vdd,
      VSS => vss,

      scin => scin,
      test => test,
      scout => scout,


      -- Instruction Cache Interface
      inst_adr => if_adr,
      inst_req => if_adr_valid,
      inst_in  => ic_inst,
      inst_valid => ivalid,

      -- Data Cache Interface
      data_adr => mem_adr,
      data_load_w => mem_load,
      data_store_b => mem_stb,
      data_store_h => mem_sth,
      data_store_w => mem_stw,
      data_out => mem_data,
      data_in => dc_data,
      data_valid => dvalid


      -- Extensions (in option, not supported here)
      --core_mode   : out std_logic_vector(1 downto 0);       -- Processor Mode (Kernel, User, etc..)
      --irq_in      : in  std_logic;                          -- Signal IRQ in
      --except_in   : in  std_logic;                          -- For exceptions (bad address, bad mode, etc...)
    );
    ivalid <= not ic_stall;
    dvalid <= not dc_stall;

    file_open(file_INPUT, "scanpath.in",  read_mode);
    file_open(file_OUTPUT, "scanpath.out",  write_mode);

-- No memory access when test insertion
if_adr_valid_not_test <= '1' when if_adr_valid = '1' and test = '0' else '0';
mem_stw_not_test <= '1' when mem_stw = '1' and test = '0' else '0';
mem_sth_not_test <= '1' when mem_sth = '1' and test = '0' else '0';
mem_stb_not_test <= '1' when mem_stb = '1' and test = '0' else '0';
mem_load_not_test <= '1' when mem_load = '1' and test = '0' else '0';

process
  variable iadr : signed(31 downto 0);
  variable v_ILINE     : line;
  variable v_OLINE     : line;
  variable scan_in : bit_vector(scan_path_size downto 0);
  variable nb_cycle : integer;
  variable scan_out_expected : bit_vector(scan_path_size downto 0);
  variable scan_output : bit_vector(scan_path_size downto 0);
begin
  GoodAdr <= std_logic_vector(TO_SIGNED(mem_goodadr, 32));
	BadAdr <= std_logic_vector(TO_SIGNED(mem_badadr, 32));


	reset_n <= '0';
	ck <= '0';
	wait for 100 ns;
	ck <= '1';
	wait for 100 ns;
	reset_n <= '1';


  readline(file_INPUT, v_ILINE);
  read(v_ILINE, scan_in);

  -- Start to insert scan value
  test <= '1';
  for i in 0 to scan_path_size loop
    report "Insert" &  integer'image(i) & " => " & bit'image(scan_in(i)) severity note;
    scin <= scan_in(i);
		ck <= '0';
		wait for 100 ns;
		ck <= '1';
		wait for 100 ns;
  end loop;
  test <= '0';
  readline(file_INPUT, v_ILINE);
  read(v_ILINE, nb_cycle);

  report "Value insered, start test";
	while not (nb_cycle = 0) loop
		ck <= '0';
		wait for 100 ns;
		ck <= '1';
		wait for 100 ns;
    nb_cycle := nb_cycle - 1;
	end loop;
  report "end test";

  test <= '1';

  for i in 0 to scan_path_size loop
    scan_output(i) := scout; -- first out is last expected
		ck <= '0';
		wait for 100 ns;
		ck <= '1';
		wait for 100 ns;
  end loop;

  readline(file_INPUT, v_ILINE);
  read(v_ILINE, scan_out_expected);

  for i in 0 to scan_path_size loop
    if scan_out_expected(i) /= scan_output(i) then
      report "FAIL : " & integer'image(i) & " bit (" & bit'image(scan_output(i)) & ") is not same than expected (" & bit'image(scan_out_expected(i)) & ")";
    end if;
  end loop;




	iadr := signed(to_stdlogicvector(if_adr));
  if if_adr = to_bitvector(GoodAdr) then
    report "GOOD!!!" severity note;
    good_end(TO_INTEGER(iadr));
    --good_end();
  end if;

  if if_adr = to_bitvector(BadAdr) then
    report "BAD!!!" severity note;
    bad_end(TO_INTEGER(iadr));
    --bad_end();
  end if;
  report "Output vector : " & to_bstring(to_stdlogicvector(scan_output)) severity note;
  write(v_OLINE, scan_output, right, scan_output'length);
  writeline(file_OUTPUT, v_OLINE);
  report "Output expect : " & to_bstring(to_stdlogicvector(scan_out_expected)) severity note;
	assert false report "end of test" severity note;

wait;
end process;

end riscV_SoftSIMU_scanpath;
