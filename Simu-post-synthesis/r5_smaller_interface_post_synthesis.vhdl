LIBRARY ieee;
use ieee.numeric_std.all;
USE ieee.std_logic_1164.all;

library work;
use work.all;
use work.ram.all;

library work;
use work.out_value.all;

entity r5_smaller_interface_post_synthesis is
end r5_smaller_interface_post_synthesis;


architecture r5_smaller_interface_post_synthesis of r5_smaller_interface_post_synthesis is

  -- We use dcache as a generic memory interface
	component dcache
  generic(
--    latency : natural
    write_latency : std_logic_vector(3 downto 0);
    read_latency : std_logic_vector(3 downto 0);
    random_latency : std_logic
  );
	port(
	-- Dcache outterface
			mem_adr			: in Std_Logic_Vector(31 downto 0);
			mem_stw			: in Std_Logic;
			mem_sth			: in Std_Logic;
			mem_stb			: in Std_Logic;
			mem_load			: in Std_Logic;

			mem_data			: in Std_Logic_Vector(31 downto 0);
			dc_data			: out Std_Logic_Vector(31 downto 0);
			dc_stall			: out Std_Logic;

			ck					: in Std_logic);
	end component;

  component r5_smaller_interface
  port ( bits_valid : in bit
       ; ck         : in bit
       ; reset_n    : in bit
       ; req        : out bit
       ; format_rw : inout bit_vector(1 downto 0)
       ; bits_in     : in  bit_vector(31 downto 0)
       ; bits_out     : inout  bit_vector(31 downto 0)
       ; bits_enable : inout bit
       ; vdd        : in bit
       ; vss        : in bit
       );
	end component;


  signal bits : bit_vector(31 downto 0);

	signal	mem_adr			: bit_Vector(31 downto 0);
	signal	mem_stw			: Std_Logic;
	signal	mem_sth			: Std_Logic;
	signal	mem_stb			: Std_Logic;
	signal	mem_load			: Std_Logic;

	signal	mem_data			: bit_Vector(31 downto 0);
	signal	dc_data			: Std_Logic_Vector(31 downto 0);
	signal	dc_stall			: Std_Logic;

	signal	ck					: bit;
	signal	reset_n			: bit;
	signal	vdd				: bit := '1';
	signal	vss				: bit := '0';

	signal	GoodAdr			: Std_Logic_Vector(31 downto 0) ;
	signal	BadAdr			: Std_Logic_Vector(31 downto 0) ;


  signal req : bit;
  signal bits_in, bits_out : bit_vector(31 downto 0);
  signal bits_enable : bit;
  signal bits_valid : bit;
  signal format_rw : bit_vector(1 downto 0);


  constant write_latency : std_logic_vector(3 downto 0) := X"0"; -- + stall for x cycle
  constant read_latency : std_logic_vector(3 downto 0) := X"0"; -- + stall for x cycle
  constant do_random : std_logic := '0';

  type state is (IDLE,WRITE, WRITE_OP, READ, READ_OP, OK);
  signal cur_state, next_state : state;

	begin
	--  Component instantiation.


	cache_i : dcache
  generic map(
    write_latency =>  write_latency,
    read_latency => read_latency,
    random_latency => do_random
  )
	port map (	mem_adr	=> to_stdlogicvector(mem_adr),
					mem_stw	=> mem_stw,
					mem_sth	=> mem_sth,
					mem_stb	=> mem_stb,
					mem_load	=> mem_load,

					mem_data	=> to_stdlogicvector(mem_data),
					dc_data	=> dc_data,
					dc_stall	=> dc_stall,
					ck			=> to_stdulogic(ck)
      );


  core_smaller : r5_smaller_interface
  port map(
      ck  => ck,
      reset_n => reset_n,
      req => req,
      format_rw => format_rw,
      bits_in => bits_in,
      bits_out => bits_out,
      bits_enable => bits_enable,
      bits_valid => bits_valid,
      VDD => vdd,
      VSS => vss
    );

-- Emulate tristate
bits <= bits_out when bits_enable = '1' else bits_in;

process (ck)
begin
--if rising_edge(ck) then -- No rising edge for bit before vhdl 2008
if ck'event and ck = '1' then
	if (reset_n = '0') then
    cur_state <= IDLE;
	else
    cur_state <= next_state;
    case next_state is
      when IDLE =>
        mem_adr <= mem_adr;
        mem_stw <= '0';
        mem_sth <= '0';
        mem_stb <= '0';
        mem_data <= mem_data;
        mem_load <= '0';
--        bits <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
        bits_valid <= '0';
      when WRITE =>
        mem_adr <= bits;
        mem_stw <= '0';
        mem_sth <= '0';
        mem_stb <= '0';
        mem_data <= mem_data;
        mem_load <= '0';
--        bits <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
        bits_valid <= '0';
      when WRITE_OP =>
        mem_adr <= mem_adr;
        if format_rw = "01" then
          mem_stw <= '1';
        else
          mem_stw <= '0';
        end if;
        if format_rw = "10" then
          mem_sth <= '1';
        else
          mem_sth <= '0';
        end if;
        if format_rw = "11" then
          mem_stb <= '1';
        else
          mem_stb <= '0';
        end if;
        mem_data <= bits;
        mem_load <= '0';
--        bits <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
        bits_valid <= '0';
      when READ =>
        mem_adr <= bits;
        mem_stw <= '0';
        mem_sth <= '0';
        mem_stb <= '0';
        mem_data <= mem_data;
        mem_load <= '1';
--        bits <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
        bits_valid <= '0';
      when READ_OP =>
        mem_adr <= mem_adr;
        mem_stw <= '0';
        mem_sth <= '0';
        mem_stb <= '0';
        mem_data <= mem_data;
        mem_load <= '0';
        bits_in <= to_bitvector(dc_data);
        bits_valid <= '0';
      when OK =>
        mem_adr <= mem_adr;
        mem_stw <= '0';
        mem_sth <= '0';
        mem_stb <= '0';
        mem_data <= mem_data;
        mem_load <= '0';
        bits_in <= bits_in;
        bits_valid <= '1';
    end case;
	end if;
end if;
end process;

process (cur_state, req, format_rw, dc_stall)
begin
	case cur_state is
  when IDLE =>
    if req = '1' then
      if format_rw = "00" then
        next_state <= READ;
      else
        next_state <= WRITE;
      end if;
    else
      next_state <= IDLE;
    end if;
  when WRITE =>
    next_state <= WRITE_OP;
  when WRITE_OP =>
    if dc_stall = '0' then
      next_state <= OK;
    else
      next_state <= cur_state;
    end if;
  when READ =>
    if dc_stall = '0' then
      next_state <= READ_OP;
    else
      next_state <= cur_state;
    end if;
  when READ_OP => -- TODO maybe this state can be remove READ_OP
    next_state <= OK;
  when OK =>
    next_state <= IDLE;
  end case;
end process;

process
  variable iadr : signed(31 downto 0);
begin
	GoodAdr <= std_logic_vector(TO_SIGNED(mem_goodadr, 32));
	BadAdr <= std_logic_vector(TO_SIGNED(mem_badadr, 32));

	reset_n <= '0';
	ck <= '0';
	wait for 100 ns;
	ck <= '1';
	wait for 100 ns;
	reset_n <= '1';
	while not (req = '1' and (mem_adr = to_bitvector(GoodAdr) or mem_adr = to_bitvector(BadAdr))) loop
		ck <= '0';
		wait for 100 ns;
		ck <= '1';
		wait for 100 ns;
	end loop;

	iadr := signed(to_stdulogicvector(mem_adr));
  if mem_adr = to_bitvector(GoodAdr) then
    report "GOOD!!!" severity note;
    good_end(TO_INTEGER(iadr));
    --good_end();
  end if;

  if mem_adr = to_bitvector(BadAdr) then
    report "BAD!!!" severity note;
    bad_end(TO_INTEGER(iadr));
    --bad_end();
  end if;

	assert false report "end of test" severity note;

wait;
end process;

end r5_smaller_interface_post_synthesis;
