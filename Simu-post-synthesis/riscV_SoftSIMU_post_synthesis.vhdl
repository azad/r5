LIBRARY ieee;
use ieee.numeric_std.all;
USE ieee.std_logic_1164.all;

library work;
use work.all;
use work.ram.all;

library work;
use work.out_value.all;

entity riscV_SoftSIMU_post_synthesis is
end riscV_SoftSIMU_post_synthesis;


architecture riscV_SoftSIMU_post_synthesis of riscV_SoftSIMU_post_synthesis is

	component icache
  generic(
    latency : natural;
    random_latency : std_logic -- latency will be random beetween 1 and latency
  );
	port(
      ck : std_logic;
	-- Icache interface
			if_adr			: in std_logic_vector(31 downto 0) ;
			if_adr_valid	: in std_logic;

			ic_inst			: out std_logic_vector(31 downto 0) ;
			ic_stall			: out std_logic);
	end component;

	component dcache
  generic(
--    latency : natural
    write_latency : std_logic_vector(3 downto 0);
    read_latency : std_logic_vector(3 downto 0);
    random_latency : std_logic
  );
	port(
	-- Dcache outterface
			mem_adr			: in std_logic_Vector(31 downto 0);
			mem_stw			: in std_logic;
			mem_sth			: in std_logic;
			mem_stb			: in std_logic;
			mem_load			: in std_logic;

			mem_data			: in std_logic_Vector(31 downto 0);
			dc_data			: out std_logic_Vector(31 downto 0);
			dc_stall			: out std_logic;

			ck					: in std_logic);
	end component;
  component riscV_Core
  port( -- Global Interface
      ck     : in bit;                               -- Core Clock
      reset_n   : in bit;                               -- Global Asynchrone Reset
      VDD     : in bit;                                     -- Power Supply VDD Voltage
      VSS     : in bit;                                     -- Power Supply VSS Voltage

      -- Instruction Cache Interface
      inst_adr    : inout bit_vector(31 downto 0);      -- Instruction address
      inst_req    : inout bit;                          -- Instruction request
      inst_in     : in  bit_vector(31 downto 0);      -- Instruction given by ICache
      inst_valid  : in  bit;                          -- Instruction valide

      -- Data Cache Interface
      data_adr    : inout bit_vector(31 downto 0);      -- Data address
      data_load_w : inout bit;
      data_store_b : inout bit;
      data_store_h : inout bit;
      data_store_w : inout bit;
      data_out    : inout bit_vector(31 downto 0);      -- Data out (Store)
      data_in     : in  bit_vector(31 downto 0);      -- Data given by DCache
      data_valid  : in  bit                           -- Data valide

      -- Extensions (in option, not supported here)
      --core_mode   : out bit_vector(1 downto 0);       -- Processor Mode (Kernel, User, etc..)
      --irq_in      : in  bit;                          -- Signal IRQ in
      --except_in   : in  bit;                          -- For exceptions (bad address, bad mode, etc...)
    );
	end component;
	signal	if_adr			: bit_Vector(31 downto 0) ;
	signal	if_adr_valid	: bit;

	signal	ic_inst			: bit_Vector(31 downto 0) ;
	signal	ic_stall			: bit;
	signal	ic_inst_sl			: std_logic_Vector(31 downto 0) ;
	signal	ic_stall_sl			: std_logic;
   
	signal	mem_adr			: bit_Vector(31 downto 0);
	signal	mem_stw			: bit;
	signal	mem_sth			: bit;
	signal	mem_stb			: bit;
	signal	mem_load			: bit;

	signal	mem_data			: bit_Vector(31 downto 0);
	signal	dc_data			: bit_Vector(31 downto 0);
	signal	dc_stall			: bit;
	signal	dc_data_sl			: std_logic_Vector(31 downto 0) ;
	signal	dc_stall_sl			: std_logic;

	signal	ck					: bit;
	signal	reset_n			: bit;
	signal	vdd				: bit := '1';
	signal	vss				: bit := '0';

	signal	GoodAdr			: std_logic_Vector(31 downto 0) ;
	signal	BadAdr			: std_logic_Vector(31 downto 0) ;

  signal dvalid : bit;
  signal ivalid : bit;
  signal mem_size : bit_vector( 2 downto 0);
  signal mem_req : bit;
  signal mem_w_not_r : bit;



  constant latency_icache : natural := 0;
  constant write_latency : std_logic_vector(3 downto 0) := X"0"; -- + stall for x cycle
  constant read_latency : std_logic_vector(3 downto 0) := X"0"; -- + stall for x cycle
  constant do_random : std_logic := '0';

	begin
	--  Component instantiation.

	icache_i : icache
  generic map(
    latency => latency_icache,
    random_latency => do_random
  )
	port map (
          ck => to_stdulogic(ck),
          if_adr			=> to_stdlogicvector(if_adr),
					if_adr_valid	=> to_stdulogic(if_adr_valid),
					ic_inst			=> ic_inst_sl,
					ic_stall			=> ic_stall_sl);
	ic_inst <= to_bitvector(ic_inst_sl);
	ic_stall <= to_bit(ic_stall_sl);

	dcache_i : dcache
  generic map(
    write_latency =>  write_latency,
    read_latency => read_latency,
    random_latency => do_random
  )
	port map (	mem_adr	=> to_stdlogicvector(mem_adr),
					mem_stw	=> to_stdulogic(mem_stw),
					mem_sth	=> to_stdulogic(mem_sth),
					mem_stb	=> to_stdulogic(mem_stb),
					mem_load	=> to_stdulogic(mem_load),

					mem_data	=> to_stdlogicvector(mem_data),
					dc_data	=> dc_data_sl,
					dc_stall	=> dc_stall_sl,
					ck			=> to_stdulogic(ck));
	dc_data <= to_bitvector(dc_data_sl);
	dc_stall <= to_bit(dc_stall_sl);
  
  core_i : riscV_Core
  port map( -- Global Interface
      ck  => ck,
      reset_n => reset_n,
      VDD => vdd,
      VSS => vss,

      -- Instruction Cache Interface
      inst_adr => if_adr,
      inst_req => if_adr_valid,
      inst_in  => ic_inst,
      inst_valid => ivalid,

      -- Data Cache Interface
      data_adr => mem_adr,
      data_load_w => mem_load,
      data_store_b => mem_stb,
      data_store_h => mem_sth,
      data_store_w => mem_stw,
      data_out => mem_data,
      data_in => dc_data,
      data_valid => dvalid

      -- Extensions (in option, not supported here)
      --core_mode   : out std_logic_vector(1 downto 0);       -- Processor Mode (Kernel, User, etc..)
      --irq_in      : in  std_logic;                          -- Signal IRQ in
      --except_in   : in  std_logic;                          -- For exceptions (bad address, bad mode, etc...)
    );
    ivalid <= not ic_stall;
    dvalid <= not dc_stall;


process
  variable iadr : signed(31 downto 0);
begin
	GoodAdr <= std_logic_vector(TO_SIGNED(mem_goodadr, 32));
	BadAdr <= std_logic_vector(TO_SIGNED(mem_badadr, 32));

	reset_n <= '0';
	ck <= '0';
	wait for 100 ns;
	ck <= '1';
	wait for 100 ns;
	reset_n <= '1';
	while not (if_adr_valid = '1' and (if_adr = to_bitvector(GoodAdr) or if_adr = to_bitvector(BadAdr))) loop
		ck <= '0';
		wait for 100 ns;
		ck <= '1';
		wait for 100 ns;
	end loop;

	iadr := signed(to_stdlogicvector(if_adr));
  if if_adr = to_bitvector(GoodAdr) then
    report "GOOD!!!" severity note;
    good_end(TO_INTEGER(iadr));
    --good_end();
  end if;

  if if_adr = to_bitvector(BadAdr) then
    report "BAD!!!" severity note;
    bad_end(TO_INTEGER(iadr));
    --bad_end();
  end if;

	assert false report "end of test" severity note;

wait;
end process;

end riscV_SoftSIMU_post_synthesis;
