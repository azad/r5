LIBRARY ieee;
USE ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity REG is
generic( D : natural := 32); -- Data length
  port(
      R1_ADR : in std_logic_vector(4 downto 0);
      R1_valid : out std_logic;
      REG_R1 : out std_logic_vector(D-1 downto 0);

      R2_ADR : in std_logic_vector(4 downto 0);
      R2_valid : out std_logic;
      REG_R2 : out std_logic_vector(D-1 downto 0);

      INVAL_ADR : in std_logic_vector(4 downto 0);
      INVAL_REQ : in std_logic;
      INVAL_VALIDITY : out std_logic;

      RD_W : in std_logic_vector(4 downto 0);
      W_ENABLE : in std_logic;
      RES_W : in std_logic_vector(D-1 downto 0);

      ck : in std_logic;
      reset_n : in std_logic;
      vdd : in bit;
      vss : in bit
  );
end REG;


architecture beh of REG is

signal in_reg0 , out_reg0  : std_logic_vector(D-1 downto 0);
signal in_reg1 , out_reg1	 : std_logic_vector(D-1 downto 0);
signal in_reg2 , out_reg2  : std_logic_vector(D-1 downto 0);
signal in_reg3 , out_reg3  : std_logic_vector(D-1 downto 0);
signal in_reg4 , out_reg4  : std_logic_vector(D-1 downto 0);
signal in_reg5 , out_reg5  : std_logic_vector(D-1 downto 0);
signal in_reg6 , out_reg6  : std_logic_vector(D-1 downto 0);
signal in_reg7 , out_reg7  : std_logic_vector(D-1 downto 0);
signal in_reg8 , out_reg8  : std_logic_vector(D-1 downto 0);
signal in_reg9 , out_reg9  : std_logic_vector(D-1 downto 0);
signal in_reg10, out_reg10 : std_logic_vector(D-1 downto 0);
signal in_reg11, out_reg11 : std_logic_vector(D-1 downto 0);
signal in_reg12, out_reg12 : std_logic_vector(D-1 downto 0);
signal in_reg13, out_reg13 : std_logic_vector(D-1 downto 0);
signal in_reg14, out_reg14 : std_logic_vector(D-1 downto 0);
signal in_reg15, out_reg15 : std_logic_vector(D-1 downto 0);
signal in_reg16, out_reg16 : std_logic_vector(D-1 downto 0);
signal in_reg17, out_reg17 : std_logic_vector(D-1 downto 0);
signal in_reg18, out_reg18 : std_logic_vector(D-1 downto 0);
signal in_reg19, out_reg19 : std_logic_vector(D-1 downto 0);
signal in_reg20, out_reg20 : std_logic_vector(D-1 downto 0);
signal in_reg21, out_reg21 : std_logic_vector(D-1 downto 0);
signal in_reg22, out_reg22 : std_logic_vector(D-1 downto 0);
signal in_reg23, out_reg23 : std_logic_vector(D-1 downto 0);
signal in_reg24, out_reg24 : std_logic_vector(D-1 downto 0);
signal in_reg25, out_reg25 : std_logic_vector(D-1 downto 0);
signal in_reg26, out_reg26 : std_logic_vector(D-1 downto 0);
signal in_reg27, out_reg27 : std_logic_vector(D-1 downto 0);
signal in_reg28, out_reg28 : std_logic_vector(D-1 downto 0);
signal in_reg29, out_reg29 : std_logic_vector(D-1 downto 0);
signal in_reg30, out_reg30 : std_logic_vector(D-1 downto 0);
signal in_reg31, out_reg31 : std_logic_vector(D-1 downto 0);

signal in_valid, out_valid : std_logic_vector(31 downto 0); -- registre valid si 1

begin

REG1_CHOICE :
REG_R1 <=  out_reg1 	when R1_ADR = "00001" else
           out_reg2 	when R1_ADR = "00010" else
           out_reg3 	when R1_ADR = "00011" else
           out_reg4 	when R1_ADR = "00100" else
           out_reg5 	when R1_ADR = "00101" else
           out_reg6 	when R1_ADR = "00110" else
           out_reg7 	when R1_ADR = "00111" else
           out_reg8 	when R1_ADR = "01000" else
           out_reg9 	when R1_ADR = "01001" else
           out_reg10	when R1_ADR = "01010" else
           out_reg11 	when R1_ADR = "01011" else
           out_reg12 	when R1_ADR = "01100" else
           out_reg13 	when R1_ADR = "01101" else
           out_reg14 	when R1_ADR = "01110" else
           out_reg15 	when R1_ADR = "01111" else
           out_reg16 	when R1_ADR = "10000" else
           out_reg17 	when R1_ADR = "10001" else
           out_reg18 	when R1_ADR = "10010" else
           out_reg19 	when R1_ADR = "10011" else
           out_reg20 	when R1_ADR = "10100" else
           out_reg21 	when R1_ADR = "10101" else
           out_reg22 	when R1_ADR = "10110" else
           out_reg23 	when R1_ADR = "10111" else
           out_reg24 	when R1_ADR = "11000" else
           out_reg25 	when R1_ADR = "11001" else
           out_reg26 	when R1_ADR = "11010" else
           out_reg27 	when R1_ADR = "11011" else
           out_reg28 	when R1_ADR = "11100" else
           out_reg29 	when R1_ADR = "11101" else
           out_reg30 	when R1_ADR = "11110" else
           out_reg31 	when R1_ADR = "11111" else
           out_reg0;

R1_valid <=  out_valid(1) 	when R1_ADR = "00001" else
           out_valid(2) 	when R1_ADR = "00010" else
           out_valid(3) 	when R1_ADR = "00011" else
           out_valid(4) 	when R1_ADR = "00100" else
           out_valid(5) 	when R1_ADR = "00101" else
           out_valid(6) 	when R1_ADR = "00110" else
           out_valid(7) 	when R1_ADR = "00111" else
           out_valid(8) 	when R1_ADR = "01000" else
           out_valid(9) 	when R1_ADR = "01001" else
           out_valid(10)	when R1_ADR = "01010" else
           out_valid(11) 	when R1_ADR = "01011" else
           out_valid(12) 	when R1_ADR = "01100" else
           out_valid(13) 	when R1_ADR = "01101" else
           out_valid(14) 	when R1_ADR = "01110" else
           out_valid(15) 	when R1_ADR = "01111" else
           out_valid(16) 	when R1_ADR = "10000" else
           out_valid(17) 	when R1_ADR = "10001" else
           out_valid(18) 	when R1_ADR = "10010" else
           out_valid(19) 	when R1_ADR = "10011" else
           out_valid(20) 	when R1_ADR = "10100" else
           out_valid(21) 	when R1_ADR = "10101" else
           out_valid(22) 	when R1_ADR = "10110" else
           out_valid(23) 	when R1_ADR = "10111" else
           out_valid(24) 	when R1_ADR = "11000" else
           out_valid(25) 	when R1_ADR = "11001" else
           out_valid(26) 	when R1_ADR = "11010" else
           out_valid(27) 	when R1_ADR = "11011" else
           out_valid(28) 	when R1_ADR = "11100" else
           out_valid(29) 	when R1_ADR = "11101" else
           out_valid(30) 	when R1_ADR = "11110" else
           out_valid(31) 	when R1_ADR = "11111" else
           out_valid(0);

REG2_CHOICE :
REG_R2 <=  out_reg1  when R2_ADR = "00001" else
           out_reg2  when R2_ADR = "00010" else
           out_reg3  when R2_ADR = "00011" else
           out_reg4  when R2_ADR = "00100" else
           out_reg5  when R2_ADR = "00101" else
           out_reg6  when R2_ADR = "00110" else
           out_reg7  when R2_ADR = "00111" else
           out_reg8  when R2_ADR = "01000" else
           out_reg9  when R2_ADR = "01001" else
           out_reg10 when R2_ADR = "01010" else
           out_reg11 when R2_ADR = "01011" else
           out_reg12 when R2_ADR = "01100" else
           out_reg13 when R2_ADR = "01101" else
           out_reg14 when R2_ADR = "01110" else
           out_reg15 when R2_ADR = "01111" else
           out_reg16 when R2_ADR = "10000" else
           out_reg17 when R2_ADR = "10001" else
           out_reg18 when R2_ADR = "10010" else
           out_reg19 when R2_ADR = "10011" else
           out_reg20 when R2_ADR = "10100" else
           out_reg21 when R2_ADR = "10101" else
           out_reg22 when R2_ADR = "10110" else
           out_reg23 when R2_ADR = "10111" else
           out_reg24 when R2_ADR = "11000" else
           out_reg25 when R2_ADR = "11001" else
           out_reg26 when R2_ADR = "11010" else
           out_reg27 when R2_ADR = "11011" else
           out_reg28 when R2_ADR = "11100" else
           out_reg29 when R2_ADR = "11101" else
           out_reg30 when R2_ADR = "11110" else
           out_reg31 when R2_ADR = "11111" else
           out_reg0;

R2_valid <=  out_valid(1) 	when R2_ADR = "00001" else
           out_valid(2) 	when R2_ADR = "00010" else
           out_valid(3) 	when R2_ADR = "00011" else
           out_valid(4) 	when R2_ADR = "00100" else
           out_valid(5) 	when R2_ADR = "00101" else
           out_valid(6) 	when R2_ADR = "00110" else
           out_valid(7) 	when R2_ADR = "00111" else
           out_valid(8) 	when R2_ADR = "01000" else
           out_valid(9) 	when R2_ADR = "01001" else
           out_valid(10)	when R2_ADR = "01010" else
           out_valid(11) 	when R2_ADR = "01011" else
           out_valid(12) 	when R2_ADR = "01100" else
           out_valid(13) 	when R2_ADR = "01101" else
           out_valid(14) 	when R2_ADR = "01110" else
           out_valid(15) 	when R2_ADR = "01111" else
           out_valid(16) 	when R2_ADR = "10000" else
           out_valid(17) 	when R2_ADR = "10001" else
           out_valid(18) 	when R2_ADR = "10010" else
           out_valid(19) 	when R2_ADR = "10011" else
           out_valid(20) 	when R2_ADR = "10100" else
           out_valid(21) 	when R2_ADR = "10101" else
           out_valid(22) 	when R2_ADR = "10110" else
           out_valid(23) 	when R2_ADR = "10111" else
           out_valid(24) 	when R2_ADR = "11000" else
           out_valid(25) 	when R2_ADR = "11001" else
           out_valid(26) 	when R2_ADR = "11010" else
           out_valid(27) 	when R2_ADR = "11011" else
           out_valid(28) 	when R2_ADR = "11100" else
           out_valid(29) 	when R2_ADR = "11101" else
           out_valid(30) 	when R2_ADR = "11110" else
           out_valid(31) 	when R2_ADR = "11111" else
           out_valid(0);

INVAL_VALIDITY <=  out_valid(1) 	when INVAL_ADR = "00001" else
           out_valid(2) 	when INVAL_ADR = "00010" else
           out_valid(3) 	when INVAL_ADR = "00011" else
           out_valid(4) 	when INVAL_ADR = "00100" else
           out_valid(5) 	when INVAL_ADR = "00101" else
           out_valid(6) 	when INVAL_ADR = "00110" else
           out_valid(7) 	when INVAL_ADR = "00111" else
           out_valid(8) 	when INVAL_ADR = "01000" else
           out_valid(9) 	when INVAL_ADR = "01001" else
           out_valid(10)	when INVAL_ADR = "01010" else
           out_valid(11) 	when INVAL_ADR = "01011" else
           out_valid(12) 	when INVAL_ADR = "01100" else
           out_valid(13) 	when INVAL_ADR = "01101" else
           out_valid(14) 	when INVAL_ADR = "01110" else
           out_valid(15) 	when INVAL_ADR = "01111" else
           out_valid(16) 	when INVAL_ADR = "10000" else
           out_valid(17) 	when INVAL_ADR = "10001" else
           out_valid(18) 	when INVAL_ADR = "10010" else
           out_valid(19) 	when INVAL_ADR = "10011" else
           out_valid(20) 	when INVAL_ADR = "10100" else
           out_valid(21) 	when INVAL_ADR = "10101" else
           out_valid(22) 	when INVAL_ADR = "10110" else
           out_valid(23) 	when INVAL_ADR = "10111" else
           out_valid(24) 	when INVAL_ADR = "11000" else
           out_valid(25) 	when INVAL_ADR = "11001" else
           out_valid(26) 	when INVAL_ADR = "11010" else
           out_valid(27) 	when INVAL_ADR = "11011" else
           out_valid(28) 	when INVAL_ADR = "11100" else
           out_valid(29) 	when INVAL_ADR = "11101" else
           out_valid(30) 	when INVAL_ADR = "11110" else
           out_valid(31) 	when INVAL_ADR = "11111" else
           out_valid(0);

REGS_process :
process(reset_n,ck)
begin
  if rising_edge(ck) then
    if reset_n = '0' then
      out_reg0 	<= X"00000000";
      out_reg1 	<= X"00000000";
      out_reg2 	<= X"00000000";
      out_reg3 	<= X"00000000";
      out_reg4 	<= X"00000000";
      out_reg5 	<= X"00000000";
      out_reg6 	<= X"00000000";
      out_reg7 	<= X"00000000";
      out_reg8 	<= X"00000000";
      out_reg9 	<= X"00000000";
      out_reg10 <= X"00000000";
      out_reg11 <= X"00000000";
      out_reg12 <= X"00000000";
      out_reg13 <= X"00000000";
      out_reg14 <= X"00000000";
      out_reg15 <= X"00000000";
      out_reg16 <= X"00000000";
      out_reg17 <= X"00000000";
      out_reg18 <= X"00000000";
      out_reg19 <= X"00000000";
      out_reg20 <= X"00000000";
      out_reg21 <= X"00000000";
      out_reg22 <= X"00000000";
      out_reg23 <= X"00000000";
      out_reg24 <= X"00000000";
      out_reg25 <= X"00000000";
      out_reg26 <= X"00000000";
      out_reg27 <= X"00000000";
      out_reg28 <= X"00000000";
      out_reg29 <= X"00000000";
      out_reg30 <= X"00000000";
      out_reg31 <= X"00000000";
      out_valid <= X"FFFFFFFF";
    else
      out_reg0 	<= in_reg0;
      out_reg1 	<= in_reg1;
      out_reg2 	<= in_reg2;
      out_reg3 	<= in_reg3;
      out_reg4 	<= in_reg4;
      out_reg5 	<= in_reg5;
      out_reg6 	<= in_reg6;
      out_reg7 	<= in_reg7;
      out_reg8 	<= in_reg8;
      out_reg9 	<= in_reg9;
      out_reg10 <= in_reg10;
      out_reg11 <= in_reg11;
      out_reg12 <= in_reg12;
      out_reg13 <= in_reg13;
      out_reg14 <= in_reg14;
      out_reg15 <= in_reg15;
      out_reg16 <= in_reg16;
      out_reg17 <= in_reg17;
      out_reg18 <= in_reg18;
      out_reg19 <= in_reg19;
      out_reg20 <= in_reg20;
      out_reg21 <= in_reg21;
      out_reg22 <= in_reg22;
      out_reg23 <= in_reg23;
      out_reg24 <= in_reg24;
      out_reg25 <= in_reg25;
      out_reg26 <= in_reg26;
      out_reg27 <= in_reg27;
      out_reg28 <= in_reg28;
      out_reg29 <= in_reg29;
      out_reg30 <= in_reg30;
      out_reg31 <= in_reg31;
      out_valid <= in_valid;
    end if;
  end if;
end process;

INVAL_REG :
-- TODO make inval AND w possible
in_valid(0) <= '1'; -- Always valid

in_valid(1) <= '1' when (W_ENABLE = '1' and RD_W = "00001" ) 		else
               '0' when INVAL_REQ = '1' and INVAL_ADR = "00001" else
               out_valid(1);

in_valid(2) <= '1' when (W_ENABLE = '1' and RD_W = "00010") 		else
               '0' when INVAL_REQ = '1' and INVAL_ADR = "00010" else
               out_valid(2);

in_valid(3) <= '1' when (W_ENABLE = '1' and RD_W = "00011") 		else
               '0' when INVAL_REQ = '1' and INVAL_ADR = "00011" else
               out_valid(3);

in_valid(4) <= '1' when (W_ENABLE = '1' and RD_W = "00100") 		else
               '0' when INVAL_REQ = '1' and INVAL_ADR = "00100" else
               out_valid(4);

in_valid(5) <= '1' when (W_ENABLE = '1' and RD_W = "00101") 		else
               '0' when INVAL_REQ = '1' and INVAL_ADR = "00101" else
               out_valid(5);

in_valid(6) <= '1' when (W_ENABLE = '1' and RD_W = "00110") 		else
               '0' when INVAL_REQ = '1' and INVAL_ADR = "00110" else
               out_valid(6);

in_valid(7) <= '1' when (W_ENABLE = '1' and RD_W = "00111") 		else
               '0' when INVAL_REQ = '1' and INVAL_ADR = "00111" else
               out_valid(7);

in_valid(8) <= '1' when (W_ENABLE = '1' and RD_W = "01000") 		else
               '0' when INVAL_REQ = '1' and INVAL_ADR = "01000" else
               out_valid(8);

in_valid(9) <= '1' when (W_ENABLE = '1' and RD_W = "01001") 		else
               '0' when INVAL_REQ = '1' and INVAL_ADR = "01001" else
               out_valid(9);

in_valid(10) <= '1' when (W_ENABLE = '1' and RD_W = "01010") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="01010" else
               out_valid(10);

in_valid(11) <= '1' when (W_ENABLE = '1' and RD_W = "01011") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="01011" else
               out_valid(11);

in_valid(12) <= '1' when (W_ENABLE = '1' and RD_W = "01100") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="01100" else
               out_valid(12);

in_valid(13) <= '1' when (W_ENABLE = '1' and RD_W = "01101") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="01101" else
               out_valid(13);

in_valid(14) <= '1' when (W_ENABLE = '1' and RD_W = "01110") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="01110" else
               out_valid(14);

in_valid(15) <= '1' when (W_ENABLE = '1' and RD_W = "01111") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="01111" else
               	out_valid(15);

in_valid(16) <= '1' when (W_ENABLE = '1' and RD_W = "10000") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="10000" else
               	out_valid(16);

in_valid(17) <= '1' when (W_ENABLE = '1' and RD_W = "10001") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="10001" else
               	out_valid(17);

in_valid(18) <= '1' when (W_ENABLE = '1' and RD_W = "10010") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="10010" else
               	out_valid(18);

in_valid(19) <= '1' when (W_ENABLE = '1' and RD_W = "10011") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="10011" else
               	out_valid(19);

in_valid(20) <= '1' when (W_ENABLE = '1' and RD_W = "10100") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="10100" else
               	out_valid(20);

in_valid(21) <= '1' when (W_ENABLE = '1' and RD_W = "10101") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="10101" else
               	out_valid(21);

in_valid(22) <= '1' when (W_ENABLE = '1' and RD_W = "10110") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="10110" else
               	out_valid(22);

in_valid(23) <= '1' when (W_ENABLE = '1' and RD_W = "10111") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="10111" else
               	out_valid(23);

in_valid(24) <= '1' when (W_ENABLE = '1' and RD_W = "11000") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="11000" else
               	out_valid(24);

in_valid(25) <= '1' when (W_ENABLE = '1' and RD_W = "11001") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="11001" else
               	out_valid(25);

in_valid(26) <= '1' when (W_ENABLE = '1' and RD_W = "11010") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="11010" else
               	out_valid(26);

in_valid(27) <= '1' when (W_ENABLE = '1' and RD_W = "11011") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="11011" else
               	out_valid(27);

in_valid(28) <= '1' when (W_ENABLE = '1' and RD_W = "11100") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="11100" else
               	out_valid(28);

in_valid(29) <= '1' when (W_ENABLE = '1' and RD_W = "11101") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="11101" else
               	out_valid(29);

in_valid(30) <= '1' when (W_ENABLE = '1' and RD_W = "11110") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="11110" else
               	out_valid(30);

in_valid(31) <= '1' when (W_ENABLE = '1' and RD_W = "11111") 		else
               	'0' when INVAL_REQ = '1' and INVAL_ADR ="11111" else
               	out_valid(31);

REGS_IN :
in_reg0 	<= out_reg0; -- Always to zero, so same value
in_reg1 	<= RES_W when out_valid(1) 	= '0' and RD_W = "00001" and W_ENABLE = '1' else out_reg1;
in_reg2 	<= RES_W when out_valid(2) 	= '0' and RD_W = "00010" and W_ENABLE = '1' else out_reg2;
in_reg3 	<= RES_W when out_valid(3) 	= '0' and RD_W = "00011" and W_ENABLE = '1' else out_reg3;
in_reg4 	<= RES_W when out_valid(4) 	= '0' and RD_W = "00100" and W_ENABLE = '1' else out_reg4;
in_reg5 	<= RES_W when out_valid(5) 	= '0' and RD_W = "00101" and W_ENABLE = '1' else out_reg5;
in_reg6 	<= RES_W when out_valid(6) 	= '0' and RD_W = "00110" and W_ENABLE = '1' else out_reg6;
in_reg7 	<= RES_W when out_valid(7) 	= '0' and RD_W = "00111" and W_ENABLE = '1' else out_reg7;
in_reg8 	<= RES_W when out_valid(8) 	= '0' and RD_W = "01000" and W_ENABLE = '1' else out_reg8;
in_reg9 	<= RES_W when out_valid(9) 	= '0' and RD_W = "01001" and W_ENABLE = '1' else out_reg9;
in_reg10 	<= RES_W when out_valid(10) = '0' and RD_W = "01010" and W_ENABLE = '1' else out_reg10;
in_reg11 	<= RES_W when out_valid(11) = '0' and RD_W = "01011" and W_ENABLE = '1' else out_reg11;
in_reg12 	<= RES_W when out_valid(12) = '0' and RD_W = "01100" and W_ENABLE = '1' else out_reg12;
in_reg13 	<= RES_W when out_valid(13) = '0' and RD_W = "01101" and W_ENABLE = '1' else out_reg13;
in_reg14 	<= RES_W when out_valid(14) = '0' and RD_W = "01110" and W_ENABLE = '1' else out_reg14;
in_reg15 	<= RES_W when out_valid(15) = '0' and RD_W = "01111" and W_ENABLE = '1' else out_reg15;
in_reg16 	<= RES_W when out_valid(16) = '0' and RD_W = "10000" and W_ENABLE = '1' else out_reg16;
in_reg17 	<= RES_W when out_valid(17) = '0' and RD_W = "10001" and W_ENABLE = '1' else out_reg17;
in_reg18 	<= RES_W when out_valid(18) = '0' and RD_W = "10010" and W_ENABLE = '1' else out_reg18;
in_reg19 	<= RES_W when out_valid(19) = '0' and RD_W = "10011" and W_ENABLE = '1' else out_reg19;
in_reg20 	<= RES_W when out_valid(20) = '0' and RD_W = "10100" and W_ENABLE = '1' else out_reg20;
in_reg21 	<= RES_W when out_valid(21) = '0' and RD_W = "10101" and W_ENABLE = '1' else out_reg21;
in_reg22 	<= RES_W when out_valid(22) = '0' and RD_W = "10110" and W_ENABLE = '1' else out_reg22;
in_reg23 	<= RES_W when out_valid(23) = '0' and RD_W = "10111" and W_ENABLE = '1' else out_reg23;
in_reg24 	<= RES_W when out_valid(24) = '0' and RD_W = "11000" and W_ENABLE = '1' else out_reg24;
in_reg25 	<= RES_W when out_valid(25) = '0' and RD_W = "11001" and W_ENABLE = '1' else out_reg25;
in_reg26 	<= RES_W when out_valid(26) = '0' and RD_W = "11010" and W_ENABLE = '1' else out_reg26;
in_reg27 	<= RES_W when out_valid(27) = '0' and RD_W = "11011" and W_ENABLE = '1' else out_reg27;
in_reg28 	<= RES_W when out_valid(28) = '0' and RD_W = "11100" and W_ENABLE = '1' else out_reg28;
in_reg29 	<= RES_W when out_valid(29) = '0' and RD_W = "11101" and W_ENABLE = '1' else out_reg29;
in_reg30 	<= RES_W when out_valid(30) = '0' and RD_W = "11110" and W_ENABLE = '1' else out_reg30;
in_reg31 	<= RES_W when out_valid(31) = '0' and RD_W = "11111" and W_ENABLE = '1' else out_reg31;

end beh;
