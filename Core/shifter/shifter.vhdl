LIBRARY ieee;
USE ieee.std_logic_1164.all;

-- Implementation of the logarithmique shifter.
-- In RISCV, shift by the lower 5 bits of the instruction only.
-- Exemple from LAMARV1.
-- https://diode.zone/videos/watch/bdab8326-af77-435b-8aa4-f039ae0c861c
--------------------------------
--| I30 | I14 | shamt/immediate | inst   |
--|---------------------------- |
--| 0   | 0   |  yes     1      | SLLI   |
--| 0   | 1   |  yes     1      | SRLI   |
--| 1   | 1   |  yes     1      | SRAI   |
--| 0   | 0   |  no      0      | SLL    |
--| 0   | 1   |  no      0      | SRL    |
--| 1   | 1   |  no      0      | SRA    |
------------------------------
entity shifter is
	generic(D : natural := 32); -- Data length

	port(	a : in Std_Logic_Vector(D-1 downto 0);		
	      shamt : in Std_Logic_Vector(4 downto 0); -- immediate active.
	      INST_14 : in Std_Logic; 
	      INST_30 : in Std_Logic;
	      RES : out Std_Logic_Vector(D-1 downto 0);
	      -- global interface
	      vdd : in  bit;
	      vss : in  bit
      );

end shifter;

Architecture beh of shifter is
	signal left_shift: std_logic;

	signal r1: std_logic_vector (31 downto 0);
	signal r2: std_logic_vector (31 downto 0);
	signal r3: std_logic_vector (31 downto 0);

	signal t0: std_logic_vector (31 downto 0);
	signal t1: std_logic_vector (31 downto 0);
	signal t2: std_logic_vector (31 downto 0);
	signal t3: std_logic_vector (31 downto 0);

	signal u0: std_logic_vector (31 downto 0);
	signal u1: std_logic_vector (31 downto 0);
	signal u2: std_logic_vector (31 downto 0);
	signal u3: std_logic_vector (31 downto 0);

	signal p0: std_logic;
	signal p1: std_logic_vector (1 downto 0);
	signal p2: std_logic_vector (3 downto 0);
	signal p3: std_logic_vector (7 downto 0);
	signal p4: std_logic_vector (15 downto 0);

	signal operation: Std_Logic;
	signal shift_val : std_logic_vector (4 downto 0);
begin

	operation <= INST_30;

	with operation select
		p0 <= a(31) when '1',
		      '0' when others;
	with operation select
		p1 <= a(31) & a(31) when '1',
		      "00" when others;
	with operation select
		p2 <= a(31) & a(31) & a(31) & a(31) when '1',
		      "0000" when others;
	with operation select
		p3 <= a(31) & a(31) & a(31) & a(31) & 
		      a(31) & a(31) & a(31) & a(31) when '1',
		      "00000000" when others;
	with operation select
		p4 <= a(31) & a(31) & a(31) & a(31) &
		      a(31) & a(31) & a(31) & a(31) &
		      a(31) & a(31) & a(31) & a(31) &
		      a(31) & a(31) & a(31) & a(31)  when '1',
		      "0000000000000000" when others;

	left_shift <= not INST_14;

	-- selection avec downto
	with shamt(0) select
		t0 <= p0 & a(31 downto 1) when '1',
		      a when others;
	with shamt(1) select
		t1 <= p1 & t0(31 downto 2) when '1',
		      t0 when others;
	with shamt(2) select
		t2 <= p2 & t1(31 downto 4) when '1',
		      t1 when others;
	with shamt(3) select
		t3 <= p3 & t2(31 downto 8) when '1',
		      t2 when others;
	with shamt(4) select
		r2 <= p4 & t3(31 downto 16) when '1',
		      t3 when others;

	with shamt(0) select
		u0 <= a(30 downto 0) & p0 when '1',
		      a when others;
	with shamt(1) select
		u1 <= u0(29 downto 0) & p1 when '1',
		      u0 when others;
	with shamt(2) select
		u2 <= u1(27 downto 0) & p2 when '1',
		      u1 when others;
	with shamt(3) select
		u3 <= u2(23 downto 0) & p3 when '1',
		      u2 when others;
	with shamt(4) select
		r3 <= u3(15 downto 0) & p4 when '1',
		      u3 when others;

	with left_shift select
		RES <= r3 when '1',
		       r2 when others;
END beh;
