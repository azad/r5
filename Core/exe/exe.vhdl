library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity EXE is
generic( 	A : natural := 32; -- Address length
				 	D : natural := 32); -- Data length
port( -- Global Interface
			ck					: in Std_Logic;
			reset_n			: in Std_Logic;
			vdd				: in bit;
			vss				: in bit;

			-- EXECUTE from DECODE Interface
      OP1 : in std_logic_vector(D-1 downto 0);
      OP2 : in std_logic_vector(D-1 downto 0);
      STORE_IN : in std_logic_vector(D-1 downto 0);
      RD_IN: in std_logic_vector(4 downto 0);

      BYPASS_1_A : in std_logic;
      BYPASS_2_A : in std_logic;
      BYPASS_STORE_A : in std_logic;

      INST_12 : in std_logic;
      INST_13 : in std_logic;
      INST_14 : in std_logic;
      INST_30 : in std_logic;
      SUB : in std_logic;

      ADD_SHIFT_or_OTHERS : in std_logic;
      ADD_or_SHIFT : in std_logic;

      MEM_W_NOT_R : in std_logic;
      MEM_OP : in std_logic;
      MEM_W : in std_logic;
      MEM_H_B : in std_logic;
      MEM_S_U : in std_logic;

			-- EXECUTE to MEMORY Interface
      DATA_ADR_2BIT : out std_logic_vector(1 downto 0);
      RD_OUT : out std_logic_vector(4 downto 0);
      STORE_OUT : out std_logic_vector(D-1 downto 0);
      RES : out std_logic_vector(D-1 downto 0);
      data_load_w : out std_logic;
      data_store_b : out std_logic;
      data_store_h : out std_logic;
      data_store_w : out std_logic;

			-- FIFO EXE_2_MEM
      exe2mem_push:  out std_logic;
      exe2mem_full : in std_logic;

			-- FIFO DEC_2_EXE
      dec2exe_pop : out std_logic;
      dec2exe_empty : in std_logic;

			-- Other
      BYPASS_VAL : in std_logic_vector(D-1 downto 0)
		);
end EXE;

architecture Behavioral of EXE is
component comparator
  port(
    a       : in  Std_Logic_Vector(31 downto 0);
    b       : in  Std_Logic_Vector(31 downto 0);
    unsigned_s       : in  Std_Logic;
    res      : out Std_Logic;
    -- global interface
    vdd       : in  bit;
    vss       : in  bit );
end component;
component shifter 
	generic(D : natural := 32); -- Data length
	port(	a : in Std_Logic_Vector(D-1 downto 0);		
		shamt : in Std_Logic_Vector(4 downto 0); -- immediate active.
		INST_14 : in Std_Logic; 
		INST_30 : in Std_Logic;
		RES : out Std_Logic_Vector(D-1 downto 0);
    -- global interface
		vdd : in  bit;
		vss : in  bit);
end component;

signal OP1_BYPASSED: std_logic_vector(D-1 downto 0);
signal OP2_BYPASSED: std_logic_vector(D-1 downto 0);

signal cmp : std_logic;
signal cmp_extend : std_logic_vector(D-1 downto 0);

signal xor_s : std_logic_vector(D-1 downto 0);
signal or_s : std_logic_vector(D-1 downto 0);
signal and_s : std_logic_vector(D-1 downto 0);

signal shifted : std_logic_vector(D-1 downto 0);
signal shamt : std_logic_vector(4 downto 0);

signal add : std_logic_vector(D-1 downto 0);
signal not_s : std_logic_vector(D-1 downto 0);
signal reversed_or_not : std_logic_vector(D-1 downto 0);

signal add_shift : std_logic_vector(D-1 downto 0);
signal other : std_logic_vector(D-1 downto 0);

signal RES_INTERNAL : std_logic_vector(D-1 downto 0);

begin
        OP1_BYPASSED <= BYPASS_VAL when BYPASS_1_A = '1' else OP1;
        OP2_BYPASSED <= BYPASS_VAL when BYPASS_2_A = '1' else OP2;

        COMPARATOR_i: comparator
        port map(
            a => OP1_BYPASSED,
            b => OP2_BYPASSED,
            unsigned_s => INST_12,
            res => cmp,
            vdd => vdd,
            vss => vss);

        cmp_extend <= "0000000000000000000000000000000" & cmp;

        xor_s <= OP1_BYPASSED xor OP2_BYPASSED;
        or_s 	<= OP1_BYPASSED or 	OP2_BYPASSED;
        and_s <= OP1_BYPASSED and OP2_BYPASSED;


        shamt <= OP2_BYPASSED(4 downto 0);
        SHIFTER_i: shifter
        generic map(32)
        port map(
            a => OP1_BYPASSED,
            shamt => shamt,
            INST_14 => INST_14,
            INST_30 => INST_30,
            RES => shifted,
            vdd => vdd,
            vss => vss);

        not_s <= not OP2_BYPASSED;
        reversed_or_not <= not_s when SUB = '1' else OP2_BYPASSED;
        add <= std_logic_vector( signed(OP1_BYPASSED) + signed(signed(reversed_or_not) + ("0000000000000000000000000000000" & SUB)));
        -- SUB indicates when we operate a substract. In this case, we must invert all OP2 bits' and add 1
        -- SUB = '1' : substract, SUB = '0' : addition

        add_shift <= shifted when ADD_or_SHIFT = '1' else add;

        other <= and_s 			when INST_12 = '1' and INST_13 = '1' and INST_14 = '1' 	else
                 or_s 			when INST_12 = '0' and INST_13 = '1' and INST_14 = '1' 	else
                 xor_s 			when INST_13 = '0' and INST_14 = '1' 										else
                 cmp_extend when INST_14 = '0'											                else
                 OP1_BYPASSED; -- Never but require
        RES_INTERNAL <= other when ADD_SHIFT_or_OTHERS = '1' else add_shift;


      exe2mem_push <= '1' when dec2exe_empty = '0' and exe2mem_full = '0' else '0';
      dec2exe_pop <= '1' when dec2exe_empty = '0' and exe2mem_full = '0' else '0';

      STORE_OUT <= BYPASS_VAL when BYPASS_STORE_A = '1' else STORE_IN;

      data_load_w <= '1' when MEM_OP = '1' and MEM_W_NOT_R = '0' else '0';
      data_store_b <= '1' when MEM_OP = '1' and MEM_W_NOT_R = '1' and MEM_H_B = '0' and MEM_W = '0' else '0';
      data_store_h <= '1' when MEM_OP = '1' and MEM_W_NOT_R = '1' and MEM_H_B = '1' else '0';
      data_store_w <= '1' when MEM_OP = '1' and MEM_W_NOT_R = '1' and MEM_W = '1' else '0';

      RD_OUT <= RD_IN;

      RES <= RES_INTERNAL;

      DATA_ADR_2BIT <= "00" when MEM_W_NOT_R = '0' else RES_INTERNAL(1 downto 0);

end Behavioral;
