library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity IFETCH is
generic( 	A : natural := 32; -- Address length
				 	D : natural := 32); -- Data length
port( -- Global Interface
      ck          : in Std_Logic;
      reset_n     : in Std_Logic;
      vdd         : in bit;
      vss         : in bit;

      -- IFETCH to ICACHE Interface
      inst_valid  : in  std_logic;                          -- Instruction valide

      -- IFETCH to DECODE Interface
      fetch_ok : out std_logic;

			-- FIFO IF_2_DEC
      if2dec_push:  out std_logic;
      if2dec_full : in std_logic;

			-- FIFO DEC_2_IF
      dec2if_pop: out std_logic;
      dec2if_empty: in std_logic

      -- Other
  );
end IFETCH;

architecture Behavioral of IFETCH is
signal inst_req_internal : std_logic;
signal fetch_ok_internal : std_logic;
begin

  fetch_ok_internal <= '1' when (inst_valid = '1' and dec2if_empty = '0') else '0';
  fetch_ok <= fetch_ok_internal;

  if2dec_push <= '1' when fetch_ok_internal = '1' and if2dec_full = '0' else '0';
  dec2if_pop <= '1' when fetch_ok_internal = '1' and if2dec_full = '0' else '0';

end Behavioral;
