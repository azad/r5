
LIBRARY ieee;
USE ieee.std_logic_1164.all;



-- Return a < b ; a and b are interpereted as signed or unsigned according to signal 'signed'
entity comparator is
  port(
    a       : in  Std_Logic_Vector(31 downto 0);
    b       : in  Std_Logic_Vector(31 downto 0);
    unsigned_s       : in  Std_Logic;
    res      : out Std_Logic;
    -- global interface
    vdd       : in  bit;
    vss       : in  bit );
end comparator;

Architecture beh of comparator is

signal p_0, p_1, p_2, p_3, p_4, p_5,p_6,p_7,p_8,p_9, p_10, p_11, p_12, p_13, p_14, p_15,p_16,p_17,p_18,p_19,  p_20, p_21, p_22, p_23, p_24, p_25,p_26,p_27,p_28,p_29, p_30, p_31: std_logic;
signal g_0, g_1, g_2, g_3, g_4, g_5,g_6,g_7,g_8,g_9, g_10, g_11, g_12, g_13, g_14, g_15,g_16,g_17,g_18,g_19,  g_20, g_21, g_22, g_23, g_24, g_25,g_26,g_27,g_28,g_29, g_30, g_31: std_logic;

signal p_0_1  : std_logic;
signal p_2_3  : std_logic;
signal p_4_5  : std_logic;
signal p_6_7  : std_logic;
signal p_8_9  : std_logic;
signal p_10_11  : std_logic;
signal p_12_13  : std_logic;
signal p_14_15  : std_logic;
signal p_16_17  : std_logic;
signal p_18_19  : std_logic;
signal p_20_21  : std_logic;
signal p_22_23  : std_logic;
signal p_24_25  : std_logic;
signal p_26_27  : std_logic;
signal p_28_29  : std_logic;
signal p_30_31  : std_logic;

signal g_0_1  : std_logic;
signal g_2_3  : std_logic;
signal g_4_5  : std_logic;
signal g_6_7  : std_logic;
signal g_8_9  : std_logic;
signal g_10_11  : std_logic;
signal g_12_13  : std_logic;
signal g_14_15  : std_logic;
signal g_16_17  : std_logic;
signal g_18_19  : std_logic;
signal g_20_21  : std_logic;
signal g_22_23  : std_logic;
signal g_24_25  : std_logic;
signal g_26_27  : std_logic;
signal g_28_29  : std_logic;
signal g_30_31  : std_logic;

signal p_0_3  : std_logic;
signal p_4_7  : std_logic;
signal p_8_11  : std_logic;
signal p_12_15  : std_logic;
signal p_16_19  : std_logic;
signal p_20_23  : std_logic;
signal p_24_27  : std_logic;
signal p_28_31  : std_logic;

signal g_0_3  : std_logic;
signal g_4_7  : std_logic;
signal g_8_11  : std_logic;
signal g_12_15  : std_logic;
signal g_16_19  : std_logic;
signal g_20_23  : std_logic;
signal g_24_27  : std_logic;
signal g_28_31  : std_logic;

signal p_0_7  : std_logic;
signal p_8_15  : std_logic;
signal p_16_23  : std_logic;
signal p_24_31  : std_logic;

signal g_0_7  : std_logic;
signal g_8_15  : std_logic;
signal g_16_23  : std_logic;
signal g_24_31  : std_logic;

signal p_0_15  : std_logic;
signal p_16_31  : std_logic;

signal g_0_15  : std_logic;
signal g_16_31  : std_logic;

signal p_0_31  : std_logic;
signal g_0_31  : std_logic;

BEGIN
p_0 <= not a(0) xor b(0);
p_1 <= not a(1) xor b(1);
p_2 <= not a(2) xor b(2);
p_3 <= not a(3) xor b(3);
p_4 <= not a(4) xor b(4);
p_5 <= not a(5) xor b(5);
p_6 <= not a(6) xor b(6);
p_7 <= not a(7) xor b(7);
p_8 <= not a(8) xor b(8);
p_9 <= not a(9) xor b(9);
p_10 <= not a(10) xor b(10);
p_11 <= not a(11) xor b(11);
p_12 <= not a(12) xor b(12);
p_13 <= not a(13) xor b(13);
p_14 <= not a(14) xor b(14);
p_15 <= not a(15) xor b(15);
p_16 <= not a(16) xor b(16);
p_17 <= not a(17) xor b(17);
p_18 <= not a(18) xor b(18);
p_19 <= not a(19) xor b(19);
p_20 <= not a(20) xor b(20);
p_21 <= not a(21) xor b(21);
p_22 <= not a(22) xor b(22);
p_23 <= not a(23) xor b(23);
p_24 <= not a(24) xor b(24);
p_25 <= not a(25) xor b(25);
p_26 <= not a(26) xor b(26);
p_27 <= not a(27) xor b(27);
p_28 <= not a(28) xor b(28);
p_29 <= not a(29) xor b(29);
p_30 <= not a(30) xor b(30);

p_31 <= not a(31) xor b(31);

g_0 <= not a(0) and b(0);
g_1 <= not a(1) and b(1);
g_2 <= not a(2) and b(2);
g_3 <= not a(3) and b(3);
g_4 <= not a(4) and b(4);
g_5 <= not a(5) and b(5);
g_6 <= not a(6) and b(6);
g_7 <= not a(7) and b(7);
g_8 <= not a(8) and b(8);
g_9 <= not a(9) and b(9);
g_10 <= not a(10) and b(10);
g_11 <= not a(11) and b(11);
g_12 <= not a(12) and b(12);
g_13 <= not a(13) and b(13);
g_14 <= not a(14) and b(14);
g_15 <= not a(15) and b(15);
g_16 <= not a(16) and b(16);
g_17 <= not a(17) and b(17);
g_18 <= not a(18) and b(18);
g_19 <= not a(19) and b(19);
g_20 <= not a(20) and b(20);
g_21 <= not a(21) and b(21);
g_22 <= not a(22) and b(22);
g_23 <= not a(23) and b(23);
g_24 <= not a(24) and b(24);
g_25 <= not a(25) and b(25);
g_26 <= not a(26) and b(26);
g_27 <= not a(27) and b(27);
g_28 <= not a(28) and b(28);
g_29 <= not a(29) and b(29);
g_30 <= not a(30) and b(30);

--g_31 <= not a(31) and b(31);
g_31 <= ((not unsigned_s) and ((not b(31)) and a(31))) or (unsigned_s and ((not a(31)) and b(31))); -- If signed (not unsigned_s), reverse entry, because 0 is more than 1

p_0_1 <= p_0 and p_1;
p_2_3 <= p_2 and p_3;
p_4_5 <= p_4 and p_5;
p_6_7 <= p_6 and p_7;
p_8_9 <= p_8 and p_9;
p_10_11 <= p_10 and p_11;
p_12_13 <= p_12 and p_13;
p_14_15 <= p_14 and p_15;
p_16_17 <= p_16 and p_17;
p_18_19 <= p_18 and p_19;
p_20_21 <= p_20 and p_21;
p_22_23 <= p_22 and p_23;
p_24_25 <= p_24 and p_25;
p_26_27 <= p_26 and p_27;
p_28_29 <= p_28 and p_29;
p_30_31 <= p_30 and p_31;

g_0_1 <= g_1 or (g_0 and p_1 );
g_2_3 <= g_3 or (g_2 and p_3 );
g_4_5 <= g_5 or (g_4 and p_5 );
g_6_7 <= g_7 or (g_6 and p_7 );
g_8_9 <= g_9 or (g_8 and p_9 );
g_10_11 <= g_11 or (g_10 and p_11 );
g_12_13 <= g_13 or (g_12 and p_13 );
g_14_15 <= g_15 or (g_14 and p_15 );
g_16_17 <= g_17 or (g_16 and p_17 );
g_18_19 <= g_19 or (g_18 and p_19 );
g_20_21 <= g_21 or (g_20 and p_21 );
g_22_23 <= g_23 or (g_22 and p_23 );
g_24_25 <= g_25 or (g_24 and p_25 );
g_26_27 <= g_27 or (g_26 and p_27 );
g_28_29 <= g_29 or (g_28 and p_29 );
g_30_31 <= g_31 or (g_30 and p_31 );


p_0_3 <= p_0_1 and p_2_3;
p_4_7 <= p_4_5 and p_6_7;
p_8_11 <= p_8_9 and p_10_11;
p_12_15 <= p_12_13 and p_14_15;
p_16_19 <= p_16_17 and p_18_19;
p_20_23 <= p_20_21 and p_22_23;
p_24_27 <= p_24_25 and p_26_27;
p_28_31 <= p_28_29 and p_30_31;


g_0_3 <= g_2_3 or (g_0_1 and p_2_3 );
g_4_7 <= g_6_7 or (g_4_5 and p_6_7 );
g_8_11 <= g_10_11 or (g_8_9 and p_10_11 );
g_12_15 <= g_14_15 or (g_12_13 and p_14_15 );
g_16_19 <= g_18_19 or (g_16_17 and p_18_19 );
g_20_23 <= g_22_23 or (g_20_21 and p_22_23 );
g_24_27 <= g_26_27 or (g_24_25 and p_26_27 );
g_28_31 <= g_30_31 or (g_28_29 and p_30_31 );


p_0_7 <= p_0_3 and p_4_7;
p_8_15 <= p_8_11 and p_12_15;
p_16_23 <= p_16_19 and p_20_23;
p_24_31 <= p_24_27 and p_28_31;

g_0_7 <= g_4_7 or (g_0_3 and p_4_7 );
g_8_15 <= g_12_15 or (g_8_11 and p_12_15 );
g_16_23 <= g_20_23 or (g_16_19 and p_20_23 );
g_24_31 <= g_28_31 or (g_24_27 and p_28_31 );

p_0_15 <= p_0_7 and p_8_15;
p_16_31 <= p_16_23 and p_24_31;

g_0_15 <= g_8_15 or (g_0_7 and p_8_15 );
g_16_31 <= g_24_31 or (g_16_23 and p_24_31 );

p_0_31 <= p_0_15 and p_16_31;
g_0_31 <= g_16_31 or (g_0_15 and p_16_31);


res <=  g_0_31;

END beh;
