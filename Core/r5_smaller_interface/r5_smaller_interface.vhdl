library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;


entity r5_smaller_interface is
port( -- Global Interface
      ck     : in std_logic;                               -- Core Clock
      reset_n   : in std_logic;                               -- Global synchrone Reset
      VDD     : in bit;                                     -- Power Supply VDD Voltage
      VSS     : in bit;                                     -- Power Supply VSS Voltage

      req    : out std_logic;                          -- request
      bits_in     : in  std_logic_vector(31 downto 0);      -- bits input output
      bits_out     : out  std_logic_vector(31 downto 0);      -- bits input output
      bits_enable : out std_logic; -- activate tri state emission
      bits_valid  : in  std_logic;                          -- bits valid

      format_rw : out std_logic_vector(1 downto 0) -- 00 = load word ; 01 : store word ; 10 store half ; 11 store byte

    );
end r5_smaller_interface;

architecture Behavioral of r5_smaller_interface is
  component riscV_Core
  port( -- Global Interface
      ck     : in std_logic;                               -- Core Clock
      reset_n   : in std_logic;                               -- Global Asynchrone Reset
      VDD     : in bit;                                     -- Power Supply VDD Voltage
      VSS     : in bit;                                     -- Power Supply VSS Voltage

      -- Instruction Cache Interface
      inst_adr    : out std_logic_vector(31 downto 0);      -- Instruction address
      inst_req    : out std_logic;                          -- Instruction request
      inst_in     : in  std_logic_vector(31 downto 0);      -- Instruction given by ICache
      inst_valid  : in  std_logic;                          -- Instruction valide

      -- Data Cache Interface
      data_adr    : out std_logic_vector(31 downto 0);      -- Data address
      data_load_w : out std_logic;
      data_store_b : out std_logic;
      data_store_h : out std_logic;
      data_store_w : out std_logic;
      data_out    : out std_logic_vector(31 downto 0);      -- Data out (Store)
      data_in     : in  std_logic_vector(31 downto 0);      -- Data given by DCache
      data_valid  : in  std_logic                           -- Data valide

      -- Extensions (in option, not supported here)
      --core_mode   : out std_logic_vector(1 downto 0);       -- Processor Mode (Kernel, User, etc..)
      --irq_in      : in  std_logic;                          -- Signal IRQ in
      --except_in   : in  std_logic;                          -- For exceptions (bad address, bad mode, etc...)
    );
	end component;
	signal	if_adr			: Std_Logic_Vector(31 downto 0) ;

	signal	ic_inst			: Std_Logic_Vector(31 downto 0) ;
	signal	ic_stall			: Std_Logic;
   
	signal	mem_adr			: Std_Logic_Vector(31 downto 0);

	signal	mem_data			: Std_Logic_Vector(31 downto 0);
	signal	dc_data			: Std_Logic_Vector(31 downto 0);
	signal	dc_stall			: Std_Logic;

  signal data_load_w,data_store_b,data_store_h,data_store_w, inst_req : std_logic;
  signal ivalid, dvalid : std_logic;

  signal reg_bits : std_logic_vector(31 downto 0);

  -- State machine 
  type state is (IDLE,SEND_ADR_I, SEND_ADR_D, SEND_DATA_D, GET_DATA_I, GET_DATA_D,ANSWERTOCORE_I,ANSWERTOCORE_D);
  signal cur_state, next_state : state;

  signal bits : std_logic_vector(31 downto 0);


begin
  core_i : riscV_Core
  port map( -- Global Interface
      ck  => ck,
      reset_n => reset_n,
      VDD => vdd,
      VSS => vss,

      -- Instruction Cache Interface
      inst_adr => if_adr,
      inst_req => inst_req,
      inst_in  => ic_inst,
      inst_valid => ivalid,

      -- Data Cache Interface
      data_adr => mem_adr,
      data_load_w => data_load_w,
      data_store_b => data_store_b,
      data_store_h => data_store_h,
      data_store_w => data_store_w,
      data_out => mem_data,
      data_in => dc_data,
      data_valid => dvalid

    );




    -- State machine ; priority on mem access
process (ck)
begin
if (rising_edge(ck)) then
	if (reset_n = '0') then
    cur_state <= IDLE;
    ic_inst <= X"00000000";
    dc_data <= X"00000000";
    req <= '0';
    reg_bits <= "00000000000000000000000000000000";
    format_rw <= "00"; -- we dont care about this value
    ivalid <= '0';
    dvalid <= '0';
	else
    cur_state <= next_state;
    case next_state is
    when IDLE =>
      ic_inst <= ic_inst;
      dc_data <= dc_data;
      req <= '0';
      reg_bits <= reg_bits;
      format_rw <= "00"; -- we dont care about this value
      ivalid <= '0';
      dvalid <= '0';
    when SEND_ADR_I =>
      ic_inst <= ic_inst;
      dc_data <= dc_data;
      req <= '1';
      reg_bits <= if_adr;
      format_rw <= "00";
      ivalid <= '0';
      dvalid <= '0';
    when SEND_ADR_D =>
      ic_inst <= ic_inst;
      dc_data <= dc_data;
      req <= '1';
      reg_bits <= mem_adr;
      if data_load_w = '1' then
        format_rw <= "00";
      elsif data_store_w = '1' then
        format_rw <= "01";
      elsif data_store_h = '1' then
        format_rw <= "10";
      elsif data_store_b = '1' then
        format_rw <= "11";
      end if;
  -- 00 = load word ; 01 : store word ; 10 store half ; 11 store byte
      ivalid <= '0';
      dvalid <= '0';
    when SEND_DATA_D =>
      ic_inst <= ic_inst;
      dc_data <= dc_data;
      req <= '1';
      reg_bits <= mem_data;
      if data_load_w = '1' then
        format_rw <= "00";
      elsif data_store_w = '1' then
        format_rw <= "01";
      elsif data_store_h = '1' then
        format_rw <= "10";
      elsif data_store_b = '1' then
        format_rw <= "11";
      end if;
      ivalid <= '0';
      dvalid <= '0';
    when GET_DATA_I =>
      ic_inst <= ic_inst;
      dc_data <= dc_data;
      req <= '1';
      reg_bits <= reg_bits;
      format_rw <= "00";
      ivalid <= '0';
      dvalid <= '0';
    when GET_DATA_D =>
      ic_inst <= ic_inst;
      dc_data <= dc_data;
      req <= '1';
      reg_bits <= reg_bits;
      format_rw <= "00";
      ivalid <= '0';
      dvalid <= '0';
    when ANSWERTOCORE_D =>
      ic_inst <= ic_inst;
      dc_data <= bits;
      req <= '0';
      reg_bits <= reg_bits;
      format_rw <= "00"; -- we dont care about this value
      ivalid <= '0';
      dvalid <= '1';
    when ANSWERTOCORE_I =>
      ic_inst <= bits;
      dc_data <= dc_data;
      req <= '0';
      reg_bits <= reg_bits;
      format_rw <= "00"; -- we dont care about this value
      ivalid <= '1';
      dvalid <= '0';
    end case;
	end if;
end if;
end process;


bits_out <= reg_bits;
bits_enable <= '1' when (reset_n = '1' and (cur_state = SEND_ADR_I or cur_state = SEND_ADR_D or cur_state = SEND_DATA_D)) else '0';
bits <= reg_bits when (reset_n = '1' and (cur_state = SEND_ADR_I or cur_state = SEND_ADR_D or cur_state = SEND_DATA_D)) else bits_in;


-- Transition part
process (cur_state, data_load_w, data_store_b, data_store_h, data_store_w, inst_req, bits_valid)
begin
	case cur_state is
  when IDLE =>
    if (data_load_w = '1' or data_store_b = '1' or data_store_h = '1' or data_store_w = '1') then -- TODO and not test for scanpath
      next_state <= SEND_ADR_D;
    elsif (data_load_w = '0' and data_store_b = '0' and data_store_h = '0' and data_store_w = '0') and inst_req = '1' then
      next_state <= SEND_ADR_I;
    else
      next_state <= IDLE;
    end if;
  when SEND_ADR_I =>
      next_state <= GET_DATA_I;
  when SEND_ADR_D =>
      if (data_load_w = '1') then
        next_state <= GET_DATA_D;
      elsif data_store_b = '1' or data_store_h = '1' or data_store_w = '1' then
        next_state <= SEND_DATA_D;
      else
        next_state <= IDLE; -- Should not be the case, but require to not generate latch
      end if;
  when SEND_DATA_D =>
      next_state <= ANSWERTOCORE_D;
  when GET_DATA_I =>
    if bits_valid = '1' then
      next_state <= ANSWERTOCORE_I;
    else
      next_state <= cur_state;
    end if;
  when GET_DATA_D =>
    if bits_valid = '1' then
      next_state <= ANSWERTOCORE_D;
    else
      next_state <= cur_state;
    end if;
  when ANSWERTOCORE_D =>
    next_state <= IDLE;
  when ANSWERTOCORE_I =>
    next_state <= IDLE;
  end case;
end process;

end Behavioral;
