library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity MEM is
generic( 	A : natural := 32; -- Address length
				 	D : natural := 32); -- Data length
port( -- Global Interface
			ck					: in Std_Logic;
			reset_n			: in Std_Logic;
			vdd				: in bit;
			vss				: in bit;

			-- MEMORY to DCACHE Interface
      data_valid  : in  std_logic;                           -- Data valid

			-- MEMORY from EXECUTE Interface
      MEM_OP : in std_logic;

			-- FIFO MEM_2_WB
      mem2wb_push:  out std_logic;
      mem2wb_full : in std_logic;

			-- FIFO EXE_2_MEM
      exe2mem_pop : out std_logic;
      exe2mem_empty : in std_logic

			-- Other
		);
end MEM;

architecture Behavioral of MEM is
begin

      mem2wb_push <= '1' when exe2mem_empty = '0' and ((data_valid = '1' and MEM_OP = '1') or (MEM_OP = '0')) else '0';

      exe2mem_pop <= '1' when exe2mem_empty = '0' and ((data_valid = '1' and MEM_OP = '1') or (MEM_OP = '0')) else '0';

end Behavioral;
