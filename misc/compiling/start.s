.text
.globl _start
_start:
  j startup
  j _bad

startup:
  la sp, AdrStack

  jal main
  j _good #TODO check return value of main
  j _bad

_bad :
  nop
_good :
  nop

 AdrStack:  .word 0x80000000
