#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <regex.h>
#include <dirent.h>
#include "ghwlib.h"


char * extract_ieme_from_regex(regmatch_t *pmatch, int i, const char * source_string){
      char * string = NULL;
      int start = pmatch[i].rm_so;
      int end = pmatch[i].rm_eo;
      size_t size = end - start;

      string = malloc(sizeof(*string) * (size + 1));
      if(string){
        strncpy (string, &source_string[start], size);
        string[size] = '\0';
      }
      return string;
}


// Get signal number from ghw file using following pattern : /([instance]/)*[signal_name](\([bit number in bit vector]\))?
// Exemple "/riscv_softsimu/core_i/fifo_dec_to_exe/din(4)" or "/riscv_softsimu/core_i/decode_stage/bypass_a"
int get_signal_number(struct ghw_hie *hie, char * signal_name){
    struct ghw_hie *hie_next;
    int err;
    int match;
    regex_t preg;
    const char * str_reg_end_of_hierarchy = "^/[^/]*/.*$";
    const char * str_reg_extract_top_hierarchy = "^/([^/]*)(/.*)$";
    const char * str_reg_extract_nb_signal_in_signal_vector = "[(]([[:digit:]]*)[)]$";

    // Regex to know if we are at end of hierarchy
    err = regcomp (&preg, str_reg_end_of_hierarchy, REG_NOSUB | REG_EXTENDED);
    if(err != 0){
      fprintf(stderr,"Regex fail\n");
      return GHW_NO_SIG;
    }
    match = regexec (&preg, signal_name, 0, NULL, 0);
    regfree(&preg);

    if(match == 0){ //if we are not at end of hierarchy
      // extract top hierarchy name
      err = regcomp (&preg, str_reg_extract_top_hierarchy, REG_EXTENDED);
      if(err != 0){
        fprintf(stderr,"Regex fail\n");
        return GHW_NO_SIG;
      }
      size_t nmatch = 0;
      regmatch_t *pmatch = NULL;
      nmatch = preg.re_nsub;
      pmatch = malloc (sizeof (*pmatch) * nmatch);
      if (!pmatch) {
        fprintf(stderr,"Malloc fail\n");
        return GHW_NO_SIG;
      }

      match = regexec (&preg, signal_name, nmatch, pmatch, 0);
      regfree (&preg);
      if(match != 0){
        fprintf(stderr,"Regex fail to extrait hierarchy info\n");
        return GHW_NO_SIG;
      }
      char * name = extract_ieme_from_regex(pmatch, 1, signal_name);
      char * child = &(signal_name[pmatch[1].rm_eo]); // rest of hierarchy
      free(pmatch);

      while(1){ // while of browse all elements
        switch (hie->kind){
          case ghw_hie_instance: // if it's a instance, check for name
            if(hie->name){
              if(strcmp(hie->name, name ) == 0){ // Check name
                int ret = get_signal_number(hie->u.blk.child, child); // Recusif call with rest of hierarchy
                free(name);
                return ret;
              }
            }
          case ghw_hie_design:
          case ghw_hie_block:
          case ghw_hie_generate_if:
          case ghw_hie_generate_for:
          case ghw_hie_process:
          case ghw_hie_package:
            hie_next = hie->brother;
            if(hie_next == NULL){
              hie_next = hie->u.blk.child;
            }
            break;
          case ghw_hie_generic:
          case ghw_hie_eos:
            free(name);
            abort ();
          default:
            hie_next = hie->brother;
            if(hie_next == NULL){
              hie_next = hie->parent;
            }
        }
        hie = hie_next;
        if(hie == NULL){
          printf("hie null :( \n");
          break;
        }
      }
      free(name);
      return GHW_NO_SIG;
    } else { //end of hierarchy
      int bit_n = 0;
      // Check and extract bit number in bit vector
      err = regcomp (&preg, str_reg_extract_nb_signal_in_signal_vector, REG_EXTENDED);
      if(err != 0){
        fprintf(stderr,"Regex fail\n");
        return GHW_NO_SIG;
      }
      size_t nmatch = 0;
      regmatch_t *pmatch = NULL;
      nmatch = preg.re_nsub;
      pmatch = malloc (sizeof (*pmatch) * nmatch);
      if (!pmatch) {
        fprintf(stderr,"Malloc fail\n");
        return GHW_NO_SIG;
      }

      match = regexec (&preg, signal_name, nmatch, pmatch, 0);
      regfree (&preg);
      if(match == 0){ // if bit number is specified, extract it, and remove it from name
        int start = pmatch[0].rm_so;
        int end = pmatch[0].rm_eo;
        signal_name[end-1] = '\0';
        bit_n = atoi(&(signal_name[start+1]));
        signal_name[start] = '\0'; // remove bit number from name
      }
      free(pmatch);

      while(1){ // Browse all signal
        switch (hie->kind){
            case ghw_hie_signal:
            case ghw_hie_port_in:
            case ghw_hie_port_out:
            case ghw_hie_port_inout:
            case ghw_hie_port_buffer:
            case ghw_hie_port_linkage:
              if(hie->name){
                if(strcmp(hie->name, &(signal_name[1]) ) == 0){ // Check name
                  unsigned int *sigs = hie->u.sig.sigs;
                  int nb_bit_sigs = 0;
                  while(sigs[nb_bit_sigs] != GHW_NO_SIG){ // reverse order, need to find lenght
                    nb_bit_sigs++;
                  }
                  return sigs[nb_bit_sigs - bit_n-1]; // Return signal nuber according bit number
                }
              }
              break;
          case ghw_hie_generic:
          case ghw_hie_eos:
            abort ();
        }
        hie = hie->brother;
        if(hie == NULL){
          break;
        }
      }
      return GHW_NO_SIG;
    }
    return GHW_NO_SIG;
}

int main(int argc, char ** argv){

  if(argc < 4){
    printf("Usage : ./read_signal [GHW file] [signal file] [output dir]\n");
    return EXIT_FAILURE;
  }
  char * dir_name = argv[3];

  DIR * dir = opendir(dir_name);
  if(dir){
    closedir(dir);
  }else{
    printf("Dir does not exist\n");
    return EXIT_FAILURE;
  }

  struct ghw_handler h;
  struct ghw_handler *hp = &h;
  hp->flag_verbose=0;

  if (ghw_open (hp, argv[1]) != 0){
    printf("CPI could not open your file %s with ghwlib\n", argv[1]);
    return EXIT_FAILURE;
  }

  if (ghw_read_base (hp) < 0){
    printf("PB when read ghw file\n");
    return EXIT_FAILURE;
  }

  //Count number of signals 
  FILE * fp = fopen(argv[2], "r");
  if(fp == NULL){
    printf("Error opening scan signal file\n");
    return EXIT_FAILURE;
  }

  int nb_signals = 0;
  char c = getc(fp); 
  while(c != EOF){
    if(c == '\n'){
      nb_signals++;
    }
    c = getc(fp);
  }
  unsigned int * signals_tab = (unsigned int *) malloc( nb_signals * sizeof(unsigned int));


  // Read signal name from file and find signal number
  rewind(fp);
  char line[1024];
  int i = 0;
  for(i = 0; i < nb_signals; i++){
    if(fgets(line,1024,fp) != NULL){
      line[strcspn(line, "\r\n")] = '\0'; // remove \n
      printf("%s : ", line);
      int signal_number = get_signal_number(hp->hie, line);
      signals_tab[i] = signal_number;
      if(line == GHW_NO_SIG){
        printf("Fail to find signal number \n", line);
        return EXIT_FAILURE;
      }else{
        printf("%d\n", signals_tab[i]);
      }
    }else{
      printf("Fail to read line %d of scan signal file\n", i);
    }
  }
  // Get ck signal number
  int ck_num = get_signal_number(hp->hie, "/riscv_softsimu_post_synthesis/ck");
  if(ck_num == GHW_NO_SIG){
    printf("Fail to find clock signal at  /riscv_softsimu_post_synthesis/ck (hardcode value) ");
    return EXIT_FAILURE;
  }

  enum ghw_sm_type sm = ghw_sm_init;
  int eof = 0;

  union ghw_val *cur_val;
  union ghw_type *cur_type;
  char in_ascii[10];


  int i_file = 0;
  char * fullpath = (char *) malloc(strlen(dir_name) +  256 + 2); // dir name + / + file_name + \0
  sprintf(fullpath,"%s/input_%d.in", dir_name, i_file);
  FILE * in_file = fopen(fullpath, "w");

  // Browser at all clock cycle
  int i_cycle = 0;
  int last_ck, new_ck = 0;
  union ghw_val *ck_val;
  union ghw_type *ck_type;
  do{
    switch (ghw_read_sm (hp, &sm)){
      case ghw_res_snapshot:
      case ghw_res_cycle:
        ck_val = hp->sigs[ck_num].val;
        ck_type = hp->sigs[ck_num].type;
        new_ck = (strcmp(ck_type->en.lits[ck_val->e8], "'1'") == 0) ? 1 : 0;


        if(last_ck == 0 && new_ck == 1){ // If rising edge, it's a new ck cycle
          i_cycle++;

  //        if (i_cycle % nb_cycle_between) == 0 {
  //        printf("At cycle %ld : \n", (long) hp->snap_time);
          for(i = 0; i < nb_signals; i++){
            if(signals_tab[i] != GHW_NO_SIG){
            cur_val = hp->sigs[signals_tab[i]].val;
            cur_type = hp->sigs[signals_tab[i]].type;
            ghw_get_value (in_ascii, 10, cur_val, cur_type);
            if((&cur_type->en)->wkt != ghw_wkt_unknown){
              fprintf(in_file,"%c",in_ascii[1]);
            }else{
              printf("\t/!\\ can't extract binary info : %d has value %s =>  ", signals_tab[i], in_ascii);
              ghw_disp_type(hp,cur_type);
            }
            }
          }
          fprintf(in_file,"\n;%ld ns?", (long) hp->snap_time);
          // close previous file
          fclose(in_file);
          i_file++;

          // Open new file
          sprintf(fullpath,"%s/input_%d.in", dir_name, i_file);
          in_file = fopen(fullpath, "w");
          // Dump signal
          for(i = 0; i < nb_signals; i++){
            if(signals_tab[i] != GHW_NO_SIG){
            cur_val = hp->sigs[signals_tab[i]].val;
            cur_type = hp->sigs[signals_tab[i]].type;
            ghw_get_value (in_ascii, 10, cur_val, cur_type);
            if((&cur_type->en)->wkt != ghw_wkt_unknown){
              fprintf(in_file,"%c",in_ascii[1]);
            }else{
              printf("\t/!\\ can't extract binary info : %d has value %s =>  ", signals_tab[i], in_ascii);
              ghw_disp_type(hp,cur_type);
            }
            }
          }
          // write number of cycle between input and output ; possible improvement : Speficy cycles between in and out ; Speficy range of cycle to extract
          fprintf(in_file, "\n1\n");
  //        fprintf(in_file, "\n%d\n",nb_cycle_between);
  //      }
        }
        last_ck = new_ck;
        break;
      case ghw_res_eof:
        eof = 1;
        break;
      default:
        abort ();
    }
  }while(!eof);

  fclose(in_file);

  // remove useless file (first and last)
  remove(fullpath); // last
  sprintf(fullpath,"%s/input_%d.in", dir_name, 0);
  remove(fullpath); // first

  free(fullpath);

  return EXIT_SUCCESS;
}
