\documentclass [11pt, a4paper]{article}
\usepackage[a4paper, left=2cm, right=2cm,textheight = 680pt]{geometry}
\usepackage[table,xcdraw]{xcolor}
\definecolor{grey}{rgb}{0.9,0.9,0.9}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[sfdefault]{ClearSans} 
\usepackage{hyperref}
\usepackage{pdfpages}
\usepackage{lastpage}
\usepackage{fancyhdr}
%\usepackage[
%    type={CC},
%    modifier={by-nc-sa},
%    version={3.0},
%]{doclicense}
 
\pagestyle{fancy}
\fancyhf{}
\lhead{R5 manual}
%\chead{ }
\rhead{Rev 0.1 - \today}

\renewcommand{\footrulewidth}{0.4pt}
%\lfoot{\doclicenseImage[imagewidth=5em]}
\rfoot{Page \thepage \hspace{1pt} of \pageref{LastPage}}

\usepackage[tocflat]{tocstyle}
\usetocstyle{standard}

% Redefinition of ToC command to get centered heading
\makeatletter
\renewcommand\tableofcontents{%
  \null\hfill\textbf{\Large\contentsname}\hfill\null\par
  \@mkboth{\MakeUppercase\contentsname}{\MakeUppercase\contentsname}%
  \@starttoc{toc}%
}
\makeatother




\begin{document}

\begin{titlepage} % Suppresses displaying the page number on the title page and the subsequent page counts as page 1
	
	%------------------------------------------------
	%	Grey title box
	%------------------------------------------------
	
	\colorbox{grey}{
		\parbox[t]{0.93\textwidth}{ 
			\parbox[t]{0.91\textwidth}{ 
				\raggedlef
				\fontsize{50pt}{80pt}\selectfont 
				\vspace{0.7cm} 
				R5 \\
				Manual\\
				\vspace{0.7cm}
			}
		}
	}

	\vfill 
	\parbox[t]{0.93\textwidth}{ 
		\raggedleft 
		\large 
		
		\href{https://framagit.org/azad/r5}{https://framagit.org/azad/r5}\\
		\hfill\rule{0.2\linewidth}{1pt}
	}
	
\end{titlepage}

\tableofcontents
\begin{center}
\Large{Document revisions}
\end{center}


\begin{table}[htb]
\centering
\begin{tabular}{|l|l|l|}
\hline
\rowcolor[HTML]{C0C0C0} 
Rev (commit id) & Date     & Description                 \\ \hline
0.1 (24bfde9f)  & May 2019 & Initial documentation of R5 \\ \hline
0.2 (TODO)      & August 2019 & TODO                     \\ \hline
                &          &                             \\ \hline
\end{tabular}
\end{table}
\newpage

\section{Introduction}
R5 is an 5 stage RISC-V\cite{risc-v} in-order 32 bits processor. ISA supported by R5 is the base integer, RV32UI from 2.2 reference \cite{waterman_risc-v_2017} of RISC-V Foundation. All user instructions are supported except for \texttt{SYSTEM} instructions, since R5 does not have support for the privileged mode, and some instructions (FENCE.I, counters, and CSR) that become extensions (no mandatory) in the next version of the specification. The \texttt{fence} instruction is implemented as a \texttt{nop}, since R5 only has one hart\footnote{Hardware Thread} and is in-order.
No exceptions nor interruptions are supported.
Extensions are not supported.
 
Here is an overview of our pipeline, a schematic with all the details can be found as annexe.
\begin{center}
    \includegraphics[width=\textwidth]{../schematic/simple_overview/simple_overview.png}
\end{center}


PC at reset is \texttt{0x08000000} and can be change in \texttt{Core/dec/dec.vhdl}.
We have tested our design with riscv-tests and on a \href{https://wiki.sipeed.com/en/tang/premier.html}{fpga}. All is synthesizable with \href{https://www-soc.lip6.fr/equipe-cian/logiciels/alliance/}{Alliance} or \href{www.clifford.at/yosys/}{Yosys} and can be place and route to produice layout with \href{https://www-soc.lip6.fr/equipe-cian/logiciels/coriolis/}{Coriolis}. More details on how to use it can be found in \texttt{README.md} in the code
% TODO global info
%Size use ; perf we got
% Some info about synthse, P&R etc...

\section{Interface of the core}
The R5 core has the following pads interfaces :
\begin{itemize}
    \item ck  (IN - 1 bit) : Clock
    \item reset\_n (IN - 1 bit) : Synchronous reset
    \item VDD (IN - 1 bit) : Power Supply VDD Voltage
    \item VSS (IN - 1 bit) : Power Supply VSS Voltage

    \item inst\_adr (OUT - 32 bits) : Instruction address
    \item inst\_req (OUT - 1 bit) : Instruction request
    \item inst\_in (IN - 32 bits) : Instruction code requested
    \item inst\_valid (IN - 1 bit) : Instruction is valid

    \item data\_adr (OUT - 32 bits) : Data address
    \item data\_load\_w (OUT - 1 bit) : Request a word load
    \item data\_store\_b (OUT - 1 bit) : Request a byte (8 bits) store
    \item data\_store\_h (OUT - 1 bit) : Request a half (16 bits) store
    \item data\_store\_w (OUT - 1 bit) : Request a word (32 bits) store
    \item data\_out (OUT - 32 bits) :  Data to store
    \item data\_in (IN - 32 bits) : Data loaded
    \item data\_valid (IN - 1 bit) : Load is valid
\end{itemize}

\section{Pipeline}
The R5 Pipeline is a synchronous pipeline. Each stage is separated with a FIFO of depth 1. It is possible to consume and produce in the same cycle.
When the stage has done his work and can commit in the next FIFO, it will produce to the next FIFO and consume inside the previous FIFO at the rising edge of clock. All stage answers in 1 clock cycle, except for \texttt{IFETCH} and \texttt{MEM} that can take more clock cycles depending of number of clock cycles memory takes to answer.
We also have bypass from \texttt{EXE} output to \texttt{EXE} and \texttt{DEC} input.

\section{IFETCH}
\texttt{IFETCH} consumes an address to fetch an instruction from \texttt{DEC} and get it from memory. \texttt{IFETCH} will forward the instruction and it's address (\texttt{PC}) to the \texttt{DEC} stage.
\section{DEC}
\texttt{DEC} is responsible for managing the flow of instructions (when either branch/jump, wait because of dependency with previous instructions or just go to pipeline) and decoding instructions to expend them to control \texttt{EXE}, \texttt{MEM} et \texttt{WB} stage. \texttt{DEC} also has a register bench with 32 registers (reg0 hardwire to 0) which have 2 read ports, 1 write port and 1 invalid port.
 
The \texttt{DEC} push to \texttt{IFETCH} address of instruction is required not just after the one that is currently decoded, but two in advance. With this we only have one instruction fetch lost on a branch/jump (but there is no delayed slot in RISC-V).
\subsection{How the next next PC is calculated}
To handle the different PCs, depending on the value of registers comparison, we calculate 3 different PC and choose later at the end of the stage (when comparison is available). The 3 different possibilities are:
\begin{itemize}
    \item PC + 8 (or +4): Instruction is not a branch nor a jump. We push PC+8 (or +4 after a branch taken/jump) to \texttt{IFETCH}, or when it's a branch not taken.
    \item PC + offset : Instruction is a \texttt{JAL} or a branch taken.
    \item Register + offset : Instruction is a \texttt{JALR}
\end{itemize}
We choose between the possibilities with a multiplexer controlled by the result of the comparison (equal or < (signed or unsigned)). If it's not a branch, the result of the comparison is pointless, so we put the right next next PC on both inputs of this multiplexer.
\subsection{How we push instructions to EXE}
Instructions are pushed to EXE only if we have OP1 and OP2 valid (from register or from bypass) and if we can invalidate register destination.
\subsection{Bypass}
We have two bypasses, from the output of \texttt{EXE} to the input of \texttt{EXE} and the input of \texttt{DEC}. These bypasses are managed in \texttt{DEC} with two 5 bits registers (to store \texttt{RD}) and 1 bit validity by register. \texttt{BYPASS\_RD\_ON\_DEC} contains RD that can be used to bypass in \texttt{DEC} stage, and \texttt{BYPASS\_RD\_WHEN\_EXE} contains RD that will be in the output of \texttt{EXE} when the current instruction in \texttt{DEC} will be in \texttt{EXE} stage. We keep track of this by recording the RD we push to \texttt{EXE} stage and watching if \texttt{EXE} stage is empty or not (to update \texttt{BYPASS\_RD\_ON\_DEC} if the instruction in \texttt{DEC} is waiting on a condition, and the instruction in \texttt{EXE} has gotten to \texttt{MEM})

\subsection{Comparator}
The comparator is able to handle signed and unsigned comparison between two 32 bits values, interpreted as signed or unsigned values. 
The comparator is logarithmic, here is an overview with an 8 bit example : 
\begin{center}
    \includegraphics[width=0.7\columnwidth]{comparatorpirouz.png}
\end{center}
Thanks to Pirouz Bazargan-Sabet for explanation.

To do signed and unsigned comparison, we only change the Generate of the sign bit (here 31). When the value is signed 1 is smaller than 0, else unsigned 0 is smaller than 1. So the Generate of bit sign become : \texttt{(UNSIGNED . $\overline{A_i}$ . $B_i$) + ( SIGNED . $A_i$ . $\overline{B_i}$)}.
The rest of comparison is still the same.

\section{EXE}
The \texttt{EXE} stage starts by updating the \texttt{OP1}, \texttt{OP2} and \texttt{STORE} values with the values of bypass if neccessary and then do these operations in parallel :
\begin{itemize}
    \item \texttt{OP1} + \texttt{reversed\_or\_not} + \texttt{CIN} :  \texttt{reversed\_or\_not} is \texttt{OP2} or \texttt{NOT OP2} if it's a \texttt{SUB} instruction (in this case \texttt{CIN} = 1)
    \item Shifter with  \texttt{OP1}, \texttt{OP2}, \texttt{SHAMT}
    \item Comparator < with \texttt{OP1}, \texttt{OP2}, signed or unsigned, the result is extended to 32 bit with a 0. Here it works in the same way than in the \texttt{DEC} stage.
    \item \texttt{OP1} AND \texttt{OP2}
    \item \texttt{OP1} OR \texttt{OP2}
    \item \texttt{OP1} XOR \texttt{OP2}
\end{itemize}
The relevant result is chosen with a multiplexer cascade.
\subsection{Shifter}
The shifter is a logarithmic shifter, we do shift right and shift left in parallel, then we chose the result at the end.
 
Take as an example shift left, if bit 4 of the shift amount is 1, this means that we will shitf at least 16 ($2^4$) positions, what will be done is to combine 16 LSB of input and 16 bit at 0 or the sign depending of arithmetic or logic shift, for 16 LSB of output. 
Then we do the same but with bit 3 of the shift amount, a shift of 8 positions, and the result of the previous shift as input.
As example this a logical shift left with a 4 bits input and a 2 bit shift amount:
\begin{center}
    \includegraphics[width=0.7\columnwidth]{shifter.png}
\end{center}
\section{MEM}
\texttt{MEM} only make requests to memory if required and produces two results to \texttt{WB} : the output of \texttt{EXE} and the result from memory.
Output bits are on register.
\section{WB}
\texttt{WB} writes back values to the register. If the instructions are a memory load, this stage also does the following (since all load requests are a request to memory to load a word).
\begin{itemize}
    \item Select which part of the word is useful in case of a \texttt{LH} or \texttt{LB} depending on the address of the request (result from \texttt{EXE})
    \item Extend or not the bit sign in case of a \texttt{LH} or \texttt{LB} 
\end{itemize}

\section{Scanpath}
It's possible to insert scanpath, after synthesis. Scanpath can write and read value for register of r5, it is use to specify a state for debugging. Note that we dont put register from 32 general purpose register inside our scanpath, so contents is undefined and must be speficy by multiple insert of scan vector. When r5 is on mode test (for scan vector insertion/output) no write or invalidation can be made to general purpose register. Also note that scan vector values dependent of synthesis result, so scan vector for synthesis with yosys is not compatible with scan vector for synthesis with Alliance (mostly because FSM state arent encode in the same way). \\
Content of scanpath (\texttt{riscv\_core[Yosys/Alliance]\_scanpath.py}:
\begin{itemize} 
  \item \texttt{dec} :
    \begin{itemize}
      \item \texttt{pushed\_to\_exe}
      \item \texttt{cur\_state} 0-1
      \item \texttt{next\_pc(0)}
      \item \texttt{bypass\_rd\_when\_exe\_valid}
      \item \texttt{bypass\_rd\_when\_exe(0)} 0-4
		  \item \texttt{bypass\_rd\_on\_dec\_valid}
      \item \texttt{bypass\_rd\_on\_dec(0)} 0-4
    \end{itemize}
  \item Fifo content of \texttt{fifo\_dec\_to\_exe} 0-115
  \item Fifo validity of \texttt{fifo\_dec\_to\_exe}

  \item Fifo content of \texttt{fifo\_dec\_to\_ifetch} 0-31
  \item Fifo validity of \texttt{fifo\_dec\_to\_ifetch}
  \item Fifo content of \texttt{fifo\_exe\_to\_mem} 0-79
  \item Fifo validity of \texttt{fifo\_exe\_to\_mem}
  \item Fifo content of \texttt{fifo\_ifetch\_to\_dec} 0-63
  \item Fifo validity of \texttt{fifo\_ifetch\_to\_dec} 
  \item Fifo content of \texttt{fifo\_mem\_to\_wb} 0-73
  \item Fifo validity of \texttt{fifo\_mem\_to\_wb} 
\end{itemize}




\newpage
\section*{Bibliography}
\addcontentsline{toc}{section}{Bibliography}

\bibliographystyle{plain}
\bibliography{zotero.bib} 

%\newpage
\section*{Annex}
\addcontentsline{toc}{section}{Annexe}
\includepdf[pages={-},angle=-90]{overview.pdf}
%TODO last update : 09/05

\newpage


\end{document}
