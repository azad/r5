#include <stdio.h>
#include "mem.h"

extern int ghdl_main(int argc, char *argv[]);

int return_value = 1; // Bad

void good_end(uint32_t adr){
//void good_end(){
  printf("GOOD : 0x%x\n", adr);
  return_value = 0;
}

void bad_end(uint32_t adr){
  printf("BAD : 0x%x\n", adr);
  return_value = 1;
}




int main(int argc, char *argv[]){

    // Chargement de la memoire
    init_mem(argv[1]);

    // Demarage de ghdl
    ghdl_main(argc - 1, argv + 1);

    mem_free();
    //return retv;
    return return_value;
}
