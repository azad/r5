
--
-- Generated by VASY
--
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY sff1_x4 IS
PORT(
  ck	: IN BIT;
  i	: IN BIT;
  q	: OUT BIT;
  vdd	 : in  BIT;
  vss	 : in  BIT
);
END sff1_x4;

ARCHITECTURE RTL OF sff1_x4 IS
  SIGNAL sff_m	: BIT;
BEGIN
  q <= sff_m;
  PROCESS ( ck )
  BEGIN
    IF  ((ck = '1') AND ck'EVENT)
    THEN sff_m <= i;
    END IF;
  END PROCESS;
END RTL;
