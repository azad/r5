library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;

library work;
use work.ram.all;

entity Icache is
  generic(
    latency : natural;
    random_latency : std_logic -- latency will be random beetween 1 and latency
  );
	port(
      ck : std_logic;
	-- Icache interface
			if_adr			: in Std_Logic_vector(31 downto 0) ;
			if_adr_valid	: in Std_Logic;

			ic_inst			: out Std_Logic_vector(31 downto 0) ;
			ic_stall			: out Std_Logic);
end Icache;

----------------------------------------------------------------------

architecture Behavior OF Icache is

signal cmp : natural := latency;
signal last_if_adr : std_logic_vector(31 downto 0);

begin

-- Gestion du PC

process (if_adr, if_adr_valid, cmp)

variable adr : signed(31 downto 0);
variable inst : signed(31 downto 0);

begin
	if (if_adr_valid = '1' and cmp = 0) then
		adr := signed(if_adr);
		inst := TO_SIGNED(mem_lw(TO_INTEGER(adr)), 32);
		ic_inst <= std_logic_vector(inst);
    ic_stall <= '0';
  else
    ic_stall <= '1';
	end if;
end process;

process(ck,if_adr,if_adr_valid)
variable seed1, seed2: positive;               -- seed values for random generator
variable rand: real;   -- random real-number value in range 0 to 1.0
variable range_of_rand : real;
variable num_random : integer;
begin
  if (rising_edge(if_adr_valid) or ((if_adr /= last_if_adr) and (if_adr_valid = '1'))) then
    if random_latency = '1' then
      uniform(seed1, seed2, rand);   -- generate random number
      range_of_rand := real(latency);
      num_random := integer(rand * range_of_rand) + 1;
      cmp <= integer(num_random);
    else
      cmp <= latency;
    end if;
    last_if_adr <= if_adr;
  elsif rising_edge(ck) and if_adr_valid = '1' then
    if cmp = 0 then
      cmp <= 0;
    else
      cmp <= cmp -1;
    end if;
  end if;
end process;


end Behavior;
