library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;


library work;
use work.ram.all;

entity Dcache is
  generic(
    write_latency : std_logic_vector(3 downto 0);
    read_latency : std_logic_vector(3 downto 0);
    random_latency : std_logic -- latency will be random beetween 1 and [write/read]_latency
  );
	port(
	-- Dcache outterface
			mem_adr			: in Std_Logic_Vector(31 downto 0);
			mem_stw			: in Std_Logic;
			mem_sth			: in Std_Logic;
			mem_stb			: in Std_Logic;
			mem_load			: in Std_Logic;

			mem_data			: in Std_Logic_Vector(31 downto 0);
			dc_data			: out Std_Logic_Vector(31 downto 0);
			dc_stall			: out Std_Logic;

			ck					: in Std_logic);

end Dcache;

----------------------------------------------------------------------

architecture Behavior OF Dcache is

signal cmp : std_logic_vector(3 downto 0) := X"0";
signal can_do_mem_op : std_logic;
type dcache_state is (IDLE,COUNT);
signal cur_state, next_state : dcache_state := IDLE;

begin

process (ck)
begin
if (rising_edge(ck)) then
		cur_state <= next_state;
end if;
end process;

process (cur_state,cmp,mem_load,mem_stb,mem_sth,mem_stw,ck,mem_adr)
variable seed1, seed2: positive;               -- seed values for random generator
variable rand: real;   -- random real-number value in range 0 to 1.0
variable range_of_rand : real;
variable num_random : integer;
begin
  case cur_state is
  when IDLE =>
    if mem_load = '1' and read_latency /= X"0" then
      next_state <= COUNT;
      if random_latency = '1' then
        uniform(seed1, seed2, rand);   -- generate random number
        range_of_rand := real(to_integer(unsigned(read_latency)));
        num_random := integer(rand * range_of_rand) + 1;
        cmp <= std_logic_vector(to_unsigned(num_random,cmp'length));
      else
        cmp <= read_latency;
      end if;
      can_do_mem_op <= '0';
    elsif (mem_stb='1' or mem_sth='1' or mem_stw='1') and write_latency /= X"0" then
      next_state <= COUNT;
      if random_latency = '1' then
        uniform(seed1, seed2, rand);   -- generate random number
        range_of_rand := real(to_integer(unsigned(write_latency)));
        num_random := integer(rand * range_of_rand) + 1;
        cmp <= std_logic_vector(to_unsigned(num_random,cmp'length));
      else
        cmp <= write_latency;
      end if;
      can_do_mem_op <= '0';
    else
      next_state <= IDLE;
      cmp <= X"0";
      if(mem_stb='1' or mem_sth='1' or mem_stw='1' or mem_load = '1') then
        can_do_mem_op <= '1';
      else
        can_do_mem_op <= '0';
      end if;
    end if;
  when COUNT =>
    if (cmp = X"0") then
      can_do_mem_op <= '1';
      next_state <= IDLE;
      cmp <= X"0";
    else
      next_state <= COUNT;
      if (rising_edge(ck)) then
        cmp <= std_logic_vector( signed(cmp) - 1);
      end if;
      can_do_mem_op <= '0';
    end if;
  end case;
end process;



process (can_do_mem_op,mem_load,mem_stb,mem_sth,mem_stw,mem_adr)
variable adr : signed(31 downto 0);
variable data : signed(31 downto 0);
variable res : integer;
begin
  if can_do_mem_op = '1' then
      if (mem_load = '1') then
--        report "mem load" severity note;
        adr := signed(mem_adr);
        data := TO_SIGNED(mem_lw(TO_INTEGER(adr)), 32);
        dc_data <= std_logic_vector(data);
      elsif mem_stw = '1' then
--        report "mem sw" severity note;
        adr := signed(mem_adr);
        data := signed(mem_data);
        res := mem_sw(TO_INTEGER(adr), TO_INTEGER(data));
      elsif mem_sth='1' then
--        report "mem sh" severity note;
        adr := signed(mem_adr);
        data := signed(mem_data);
        res := mem_sh(TO_INTEGER(adr), TO_INTEGER(data));
      elsif mem_stb='1' then
--        report "mem sb" severity note;
        adr := signed(mem_adr);
        data := signed(mem_data);
        res := mem_sb(TO_INTEGER(adr), TO_INTEGER(data));
      end if;
      dc_stall <= '0';
  else
    dc_stall <= '1';
  end if;
end process;

end Behavior;
