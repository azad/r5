SOFT_SYNTHESIS = Yosys
#SOFT_SYNTHESIS = Alliance
SYNTHESIS_DIR = ../with_alliance-check-toolkit

GHDL=ghdl
GHDL_OP = -v -fPIC # Need when link with C
SOFT_SIMU_DIR = ../misc-simulators/Soft_SIMU/

${SOFT_SIMU_DIR}/ReadElf/src/ElfObj.o:
	$(MAKE) -C ${SOFT_SIMU_DIR}/ReadElf/src

${SOFT_SIMU_DIR}/soft/mem.o ${SOFT_SIMU_DIR}/soft/riscv_ghdl.o:
	$(MAKE) -C  ${SOFT_SIMU_DIR}/soft


ICACHE.o :  ../misc-simulators/ICACHE/ICACHE.vhdl
	${GHDL} -a ${GHDL_OP} $<
DCACHE.o :  ../misc-simulators/DCACHE/DCACHE.vhdl
	${GHDL} -a ${GHDL_OP} $<
RAM.o :  ../misc-simulators/RAM/RAM.vhdl
	${GHDL} -a ${GHDL_OP} $<
out_value.o :  ../misc-simulators/out_value.vhdl
	${GHDL} -a ${GHDL_OP} $<
std_logic_textio.o :  ../misc-simulators/std_logic_textio/std_logic_textio.vhdl
	${GHDL} -a ${GHDL_OP} $<

one_x0.o : ../misc-simulators/fix_cells/one_x0.vbe
	${GHDL} -a ${GHDL_OP} $<
zero_x0.o : ../misc-simulators/fix_cells/zero_x0.vbe
	${GHDL} -a ${GHDL_OP} $<
sff1_x4.o: ../misc-simulators/fix_cells/sff1_x4.vhd
	${GHDL} -a ${GHDL_OP} $<
sff2_x4.o: ../misc-simulators/fix_cells/sff2_x4.vhd
	${GHDL} -a ${GHDL_OP} $<
sff3_x4.o: ../misc-simulators/fix_cells/sff3_x4.vhd
	${GHDL} -a ${GHDL_OP} $<
rowend_x0.o: ../misc-simulators/fix_cells/rowend_x0.vbe
	${GHDL} -a ${GHDL_OP} $<
tie_x0.o: ../misc-simulators/fix_cells/tie_x0.vbe
	${GHDL} -a ${GHDL_OP} $<

%.o : %.vst
	${GHDL} -a ${GHDL_OP} $<
%.o : ${MBK_TARGET_LIB}/%.vbe
	${GHDL} -a ${GHDL_OP} $<

%.vst :: $(SYNTHESIS_DIR)/%.vst
	-cp $(SYNTHESIS_DIR)/$@ .

