#!/usr/bin/env python
# -*- coding: utf-8 -*-

try:
  import sys
  import traceback
  import os.path
  import shutil
  import optparse
  import math
  import Cfg
  import Hurricane
  from   Hurricane import DataBase
  from   Hurricane import DbU
  from   Hurricane import Transformation
  from   Hurricane import Box
  from   Hurricane import UpdateSession
  from   Hurricane import Breakpoint
  from   Hurricane import Net
  from   Hurricane import NetExternalComponents
  from   Hurricane import BasicLayer
  from   Hurricane import ContactLayer
  from   Hurricane import ViaLayer
  from   Hurricane import RegularLayer
  from   Hurricane import TransistorLayer
  from   Hurricane import DiffusionLayer
  from   Hurricane import Cell
  from   Hurricane import Instance
  from   Hurricane import Net
  from   Hurricane import Contact
  from   Hurricane import Horizontal
  from   Hurricane import Vertical
  import Viewer
  import CRL
  from   CRL import RoutingLayerGauge
  import helpers
  from   helpers import trace
  from   helpers.io import ErrorMessage
except ImportError, e:
  serror = str(e)
  if serror.startswith('No module named'):
    module = serror.split()[-1]
    print '[ERROR] The <%s> python module or symbol cannot be loaded.' % module
    print '        Please check the integrity of the <coriolis> package.'
  if str(e).find('cannot open shared object file'):
    library = serror.split(':')[0]
    print '[ERROR] The <%s> shared library cannot be loaded.' % library
    print '        Under RHEL 6, you must be under devtoolset-2.'
    print '        (scl enable devtoolset-2 bash)'
  sys.exit(1)
except Exception, e:
  print '[ERROR] A strange exception occurred while loading the basic Coriolis/Python'
  print '        modules. Something may be wrong at Python/C API level.\n'
  print '        %s' % e
  sys.exit(2)


framework = CRL.AllianceFramework.get()


def vst2ghdl( cell, topCell ):
    print(cell.getName())
    # Some net, have same name than instantiation name ; rename instantiation (Synthesis with Alliance)
    for instance in cell.getInstances():
        if instance.getName() == cell.getName() : #getInstances include current cell ?
            continue
#        if instance.getName() in [net.getName() for net in cell.getNets()]:
        if cell.getNet(instance.getName()) != None:
            instance.setName(instance.getName() + "_renamed")
        if instance.getMasterCell().getLibrary().getName() not in ["sxlib", "nsxlib"] : # ou = "working"
            vst2ghdl(instance.getMasterCell(), False)
    # With GHDL (and VHDL < 2008) we can not read OUT signal inside design, so we change signal to INOUT
    for net in cell.getNets():
        if not net.isExternal():
            continue
        if net.getDirection() == Net.Direction.IN:
            continue
        count=0
        for plug in net.getPlugs():
            if plug.getMasterNet().getDirection() == Net.Direction.INOUT: # When master celll have should plug to INOUT
                count=10
            count+=1
        if count > 1 :
            net.setDirection(Net.Direction.INOUT)
            continue

    if cell.getLibrary().getName() not in ["sxlib", "nsxlib"] : # ou == "working" ?
        framework.saveCell(cell, CRL.Catalog.State.Logical)
    return 1


def ScriptMain ( **kw ):
  global framework

  helpers.staticInitialization( quiet=True )

  scaledDir = framework.getAllianceLibrary(0).getPath()
  alibrary  = framework.getAllianceLibrary(1)

  sourceCell = None
  if kw.has_key('cell') and kw['cell']:
    sourceCell = kw['cell']

  if sourceCell:
    vst2ghdl( sourceCell, True )
  return 1


if __name__ == '__main__':
  parser = optparse.OptionParser()
  parser.add_option( '-c', '--cell', type='string',                      dest='cell'       , help='The name of the chip to build, whithout extension.')
  parser.add_option( '-v', '--verbose'            , action='store_true', dest='verbose'    , help='First level of verbosity.')
  parser.add_option( '-V', '--very-verbose'       , action='store_true', dest='veryVerbose', help='Second level of verbosity.')
  (options, args) = parser.parse_args()

  kw = {}
  if options.cell:
    kw['cell'] = framework.getCell( options.cell, CRL.Catalog.State.Views )
  if options.verbose:     Cfg.getParamBool('misc.verboseLevel1').setBool(True)
  if options.veryVerbose: Cfg.getParamBool('misc.verboseLevel2').setBool(True)

  print framework.getEnvironment().getPrint()

  success = ScriptMain( **kw )
  print(success)
  shellSuccess = 0
  if not success: shellSuccess = 1
  if not success:
    print("Not sucess")

  sys.exit( shellSuccess )
