# R5

This repository contain R5 source code. It's a 32 bit RISC-V processor with integer base only. There is not privilege support and not support for SYSTEM instructions, exceptions,interruptions.

The documentation of internal implementation is in `docs/`.


## `Core/`
`Core/` contain VHDL source code for R5.
You can build (ghdl) it with `make`

## `Simu/`
`Simu/` make a plateform to test R5, it's Core of R5 + "cache" to read from ELF. All of this is done with GHDL binding C (requiring [ghdl with gcc backend](https://ghdl.readthedocs.io/en/latest/building/gcc/index.html)).

`make` will produice `riscv_softsimu` which take as argument a RISCV ELF. Stack is at `0x10000000` (`misc-simulators/Soft_SIMU/soft/mem.c`), Start adress is `0x08000000` (`Core/dec/dec/vhdl`) (also see `misc/compiling/link.ld` and `misc/compling/start.s`). Simulation stop when Core ask for adress of label `_good`or `_bad`.  Exit status is 0 if simulation reach `_good`.

## `with-alliance-check-toolkit/`

This directory containt all you need to synthesis, insert scanpath and place&route. This is done by [Coriolis](https://www-soc.lip6.fr/equipe-cian/logiciels/coriolis/) and/or [Alliance](https://www-soc.lip6.fr/equipe-cian/logiciels/alliance/) and/or [Yosys](http://www.clifford.at/yosys/). You can configure Alliance or Yosys for synthesis by edit `LOGICAL_SYNTHESIS` variable in `Makefile` file (please also edit `SOFT_SYNTHESIS` in `misc-simulators/base.mk` if you want to make simulation with synthesis result)
`ioring.py` describe PAD placement and connection.

Usage :
  - `make synth` do synthesis (Note : With Alliance, dependecies fail, please just do 2 time `make synth`)
  - `make scanpath` do scanpath insertion after synthesis
  - `make layout` do place&route

This require [Alliance check toolkit](https://www-soc.lip6.fr/git/alliance-check-toolkit.git).  Alliance check toolkit can get with git submodule => `git submodule update --init --recursive`, or by default in `$(HOME)/coriolis-2.x/src/alliance-check-toolkit/`

## `riscv-tests/`
This directory is an adaptation of [riscv-tests](https://github.com/riscv/riscv-tests) to our plateform (mostly add `_good` and `_bad` label to stop simulation)

  - `make run-simu` : Launch riscv-test (from `launch-test.sh`) on Simulator
  - `make run-post-synthesis` : Launch riscv-test (from `launch-test-post-synthesis.sh`) on Simulator after synthesis
  - `make run-cts` : Launch riscv-test (from `launch-testècts.sh`) on Simulator after Clock tree insertion

## `misc/`
Random tools we use :
  - `compiling` : Makefile to compile binary for R5
  - `ghwlib` : Lib ghw from ghdl usse for `calc_cpi` and `read_signal`
  - `calc_cpi` : C programm to calculate CPI from GHW trace
  - `read_signal` : C programm to extract value of signals from GHW trace

## `misc-simulator`
This directory containt all files use by all simulators :

  - `base.mk` : Makefile with usefull rules
  - `getCellFromCellLib.py` : Extract dependencies between cell design and cell in celllib
  - `vst2ghdl.py` : Fix vst to pass in ghdl : change instance name than sometime conflit with signal name and 
  - `fix_cells/` : containt cells corrected to pass in ghdl
  - `DCACHE/` : read/write from/to ELF file and stack
  - `ICACHE/` : read instruction from ELF file
  - `RAM/` : use to interact with C code
  - `Soft_SIMU/` : C code to interact with ELF and start ghdl
  - `out_value.vhdl` : Code to register if simulation is OK or KO

Note : Some change are made on cells to make output from Alliance valid for ghdl. And output of synthesis is edit to pass in ghdl (instance cannot have same name than signal for Alliance, and yosys+ blif2vst for coriolis have lot of non standard vhdl)

## `Simu-post-synthesis/`
Use to make simulation after synthesis, it's done by making binary of simulator with sxlib/nsxlib cells (place `MBK_TARGET_LIB` env variable to choose), and post synthesis result.
Usage is the same than `Simu/` but take more time, since all cells are emulate

## `Simu-scanpath/`
Simulation after synthesis and after scan path insertion to test scanpath insertion to test scanpath insertion.

  - `riscv_softsimu_scanpath` : Read scan vector from `scanpath.in` file, insert it, execute core for X cycle (specify by `scanpath.in`), extract result and compare it with expected vector. Save result to `scanpath.out`
  - `riscv_softsimu_scanpath_for` : Same but read `scanpath_all.in` where there are multiple vector, insert, execute, extract and repeat with next. This is usefull to put state in Core (since register bench is not on scanpath)

2 usefull tools :
  - `CmpOutput.py` :
    - Usage : `python CmpOutput.py ../with_alliance-check-toolkit/riscv_core_Yosys_scanpath.py scanpath.in scanpath.out`
    - Show difference between output we get and output we want, associate with signal name
  - `ShowVal.py` :
    - Usage : `python ShowVal.py ../with_alliance-check-toolkit/riscv_core_Yosys_scanpath.py scanpath.in scanpath.out`
    - For all bits in scanpath vector show : what is insered, what we want, what we got, signal name (bit number)

Please remember that general purpose register aren't in scanpath, so we must have good value put in them, otherwise fail (value read is not value expected)

### Generate scanpath vector from binary

To generate `scanpath.in` files, we can extract values from execution on simulator after synthesis. `make_scanpath_input_vector.sh` generate all in vector for an execution, 1 input file for each cycle.

Requirement : Simulator post-synthesis available, and not that we extract value from execution on simulator post-synthesis which mean :
  - Slower execution
  - We do it automatically but we use full path of signals (including path from simulation design which is not in `riscv_core`), we prefix paths with `riscv_softsimu_post_synthesis/core_i`. If it's not your case change it in `make_scanpath_input_vector.sh` (you can list full path for all signal of a ghw file with `ghwdump -H [GHW file]`)
  - `misc/read_signal/read_signal` : C program that extract value for GHW trace

Usage of `make_scanpath_input_vector.sh`: `bash make_scanpath_input_vector.sh binary dir_for_vector scanpath_config.path`

  - `binary` : Riscv-binary
  - `dir_for_vector` : Directory to write vector input files
  - `scanpath_config.path` : File generate when scanpath insert with full path inside `riscv_core`

Exemple : `bash make_scanpath_input_vector.sh ../riscv-tests/riscv-tests/isa/bin/rv32ui-p-addi test_addi ../with_alliance-check-toolkit/riscv_core_signal.path`

## `Simu-cts/`

Simulation after synthesis and after clock tree insertion, usefull to verify clock tree is OK.
Currently not up-to-date

## Note on `r5-smaller-interface`
No use anymore, but was a prototype to reduice from 2 memory port to 1 with sequential transmission of address, then data out. (or adresse then data in + valid bit). It was here to reduice number of pad, since we finally not need it, it's not used, but it's still exist if require...
