LIBRARY ieee;
use ieee.numeric_std.all;
USE ieee.std_logic_1164.all;

library work;
use work.all;
use work.ram.all;

library work;
use work.out_value.all;

entity r5_smaller_interface_SoftSIMU is
end r5_smaller_interface_SoftSIMU;


architecture r5_smaller_interface_SoftSIMU of r5_smaller_interface_SoftSIMU is

  -- We use dcache as a generic memory interface
	component dcache
  generic(
--    latency : natural
    write_latency : std_logic_vector(3 downto 0);
    read_latency : std_logic_vector(3 downto 0);
    random_latency : std_logic
  );
	port(
	-- Dcache outterface
			mem_adr			: in Std_Logic_Vector(31 downto 0);
			mem_stw			: in Std_Logic;
			mem_sth			: in Std_Logic;
			mem_stb			: in Std_Logic;
			mem_load			: in Std_Logic;

			mem_data			: in Std_Logic_Vector(31 downto 0);
			dc_data			: out Std_Logic_Vector(31 downto 0);
			dc_stall			: out Std_Logic;

			ck					: in Std_logic);
	end component;

  component r5_smaller_interface
  port( -- Global Interface
      ck     : in std_logic;                               -- Core Clock
      reset_n   : in std_logic;                               -- Global synchrone Reset
      VDD     : in bit;                                     -- Power Supply VDD Voltage
      VSS     : in bit;                                     -- Power Supply VSS Voltage

      req    : out std_logic;                          -- request
      bits_in     : in  std_logic_vector(31 downto 0);      -- bits input output
      bits_out     : out  std_logic_vector(31 downto 0);      -- bits input output
      bits_enable : out std_logic; -- activate tri state emission
      bits_valid  : in  std_logic;                          -- bits valid

      format_rw : out std_logic_vector(1 downto 0) -- 00 = load word ; 01 : store word ; 10 store half ; 11 store byte

    );
	end component;


  signal bits : std_logic_vector(31 downto 0);
	signal	mem_adr			: Std_Logic_Vector(31 downto 0);
	signal	mem_stw			: Std_Logic;
	signal	mem_sth			: Std_Logic;
	signal	mem_stb			: Std_Logic;
	signal	mem_load			: Std_Logic;

	signal	mem_data			: Std_Logic_Vector(31 downto 0);
	signal	dc_data			: Std_Logic_Vector(31 downto 0);
	signal	dc_stall			: Std_Logic;

	signal	ck					: Std_Logic;
	signal	reset_n			: Std_Logic;
	signal	vdd				: bit := '1';
	signal	vss				: bit := '0';

	signal	GoodAdr			: Std_Logic_Vector(31 downto 0) ;
	signal	BadAdr			: Std_Logic_Vector(31 downto 0) ;


  signal req : std_logic;
  signal bits_in, bits_out : std_logic_vector(31 downto 0);
  signal bits_enable : std_logic;
  signal bits_valid : std_logic;
  signal format_rw : std_logic_vector(1 downto 0);


  constant write_latency : std_logic_vector(3 downto 0) := X"0"; -- + stall for x cycle
  constant read_latency : std_logic_vector(3 downto 0) := X"0"; -- + stall for x cycle
  constant do_random : std_logic := '0';

  type state is (IDLE,WRITE, WRITE_OP, READ, READ_OP, OK);
  signal cur_state, next_state : state;

	begin
	--  Component instantiation.


	cache_i : dcache
  generic map(
    write_latency =>  write_latency,
    read_latency => read_latency,
    random_latency => do_random
  )
	port map (	mem_adr	=> mem_adr,
					mem_stw	=> mem_stw,
					mem_sth	=> mem_sth,
					mem_stb	=> mem_stb,
					mem_load	=> mem_load,

					mem_data	=> mem_data,
					dc_data	=> dc_data,
					dc_stall	=> dc_stall,
					ck			=> ck);


  core_smaller : r5_smaller_interface
  port map( -- Global Interface
      ck  => ck,
      reset_n => reset_n,
      VDD => vdd,
      VSS => vss,

      req => req,
      bits_in => bits_in,
      bits_out => bits_out,
      bits_enable => bits_enable,
      bits_valid => bits_valid,
      format_rw => format_rw
    );


-- Emulate tristate
bits <= bits_out when bits_enable = '1' else bits_in;

process (ck)
begin
if (rising_edge(ck)) then
	if (reset_n = '0') then
    cur_state <= IDLE;
	else
    cur_state <= next_state;
    case next_state is
      when IDLE =>
        mem_adr <= mem_adr;
        mem_stw <= '0';
        mem_sth <= '0';
        mem_stb <= '0';
        mem_data <= mem_data;
        mem_load <= '0';
        bits_in <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
        bits_valid <= '0';
      when WRITE =>
        mem_adr <= bits;
        mem_stw <= '0';
        mem_sth <= '0';
        mem_stb <= '0';
        mem_data <= mem_data;
        mem_load <= '0';
        bits_in <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
        bits_valid <= '0';
      when WRITE_OP =>
        mem_adr <= mem_adr;
        if format_rw = "01" then
          mem_stw <= '1';
        else
          mem_stw <= '0';
        end if;
        if format_rw = "10" then
          mem_sth <= '1';
        else
          mem_sth <= '0';
        end if;
        if format_rw = "11" then
          mem_stb <= '1';
        else
          mem_stb <= '0';
        end if;
        mem_data <= bits;
        mem_load <= '0';
        bits_in <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
        bits_valid <= '0';
      when READ =>
        mem_adr <= bits;
        mem_stw <= '0';
        mem_sth <= '0';
        mem_stb <= '0';
        mem_data <= mem_data;
        mem_load <= '1';
        bits_in <= "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";
        bits_valid <= '0';
      when READ_OP =>
        mem_adr <= mem_adr;
        mem_stw <= '0';
        mem_sth <= '0';
        mem_stb <= '0';
        mem_data <= mem_data;
        mem_load <= '0';
        bits_in <= dc_data;
        bits_valid <= '0';
      when OK =>
        mem_adr <= mem_adr;
        mem_stw <= '0';
        mem_sth <= '0';
        mem_stb <= '0';
        mem_data <= mem_data;
        mem_load <= '0';
        bits_in <= bits_in;
        bits_valid <= '1';
    end case;
	end if;
end if;
end process;

process (cur_state, req, format_rw, dc_stall)
begin
	case cur_state is
  when IDLE =>
    if req = '1' then
      if format_rw = "00" then
        next_state <= READ;
      else
        next_state <= WRITE;
      end if;
    else
      next_state <= IDLE;
    end if;
  when WRITE =>
    next_state <= WRITE_OP;
  when WRITE_OP =>
    if dc_stall = '0' then
      next_state <= OK;
    else
      next_state <= cur_state;
    end if;
  when READ =>
    if dc_stall = '0' then
      next_state <= READ_OP;
    else
      next_state <= cur_state;
    end if;
  when READ_OP => -- TODO maybe this state can be remove READ_OP
    next_state <= OK;
  when OK =>
    next_state <= IDLE;
  end case;
end process;

process
  variable iadr : signed(31 downto 0);
begin
	GoodAdr <= std_logic_vector(TO_SIGNED(mem_goodadr, 32));
	BadAdr <= std_logic_vector(TO_SIGNED(mem_badadr, 32));

	reset_n <= '0';
	ck <= '0';
	wait for 1 ns;
	ck <= '1';
	wait for 1 ns;
	reset_n <= '1';
	while not (req = '1' and (mem_adr = GoodAdr or mem_adr = BadAdr)) loop
		ck <= '0';
		wait for 1 ns;
		ck <= '1';
		wait for 1 ns;
	end loop;

	iadr := signed(mem_adr);
  if mem_adr = GoodAdr then
    report "GOOD!!!" severity note;
    good_end(TO_INTEGER(iadr));
    --good_end();
  end if;

  if mem_adr = BadAdr then
    report "BAD!!!" severity note;
    bad_end(TO_INTEGER(iadr));
    --bad_end();
  end if;

	assert false report "end of test" severity note;

wait;
end process;

end r5_smaller_interface_SoftSIMU;
