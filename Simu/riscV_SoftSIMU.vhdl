LIBRARY ieee;
use ieee.numeric_std.all;
USE ieee.std_logic_1164.all;

library work;
use work.all;
use work.ram.all;

library work;
use work.out_value.all;

entity riscV_SoftSIMU is
end riscV_SoftSIMU;


architecture riscV_SoftSIMU of riscV_SoftSIMU is

	component icache
  generic(
    latency : natural;
    random_latency : std_logic -- latency will be random beetween 1 and latency
  );
	port(
      ck : std_logic;
	-- Icache interface
			if_adr			: in Std_Logic_vector(31 downto 0) ;
			if_adr_valid	: in Std_Logic;

			ic_inst			: out Std_Logic_vector(31 downto 0) ;
			ic_stall			: out Std_Logic);
	end component;

	component dcache
  generic(
--    latency : natural
    write_latency : std_logic_vector(3 downto 0);
    read_latency : std_logic_vector(3 downto 0);
    random_latency : std_logic
  );
	port(
	-- Dcache outterface
			mem_adr			: in Std_Logic_Vector(31 downto 0);
			mem_stw			: in Std_Logic;
			mem_sth			: in Std_Logic;
			mem_stb			: in Std_Logic;
			mem_load			: in Std_Logic;

			mem_data			: in Std_Logic_Vector(31 downto 0);
			dc_data			: out Std_Logic_Vector(31 downto 0);
			dc_stall			: out Std_Logic;

			ck					: in Std_logic);
	end component;
  component riscV_Core
  port( -- Global Interface
      ck     : in std_logic;                               -- Core Clock
      reset_n   : in std_logic;                               -- Global Asynchrone Reset
      VDD     : in bit;                                     -- Power Supply VDD Voltage
      VSS     : in bit;                                     -- Power Supply VSS Voltage

      -- Instruction Cache Interface
      inst_adr    : out std_logic_vector(31 downto 0);      -- Instruction address
      inst_req    : out std_logic;                          -- Instruction request
      inst_in     : in  std_logic_vector(31 downto 0);      -- Instruction given by ICache
      inst_valid  : in  std_logic;                          -- Instruction valide

      -- Data Cache Interface
      data_adr    : out std_logic_vector(31 downto 0);      -- Data address
      data_load_w : out std_logic;
      data_store_b : out std_logic;
      data_store_h : out std_logic;
      data_store_w : out std_logic;
      data_out    : out std_logic_vector(31 downto 0);      -- Data out (Store)
      data_in     : in  std_logic_vector(31 downto 0);      -- Data given by DCache
      data_valid  : in  std_logic                           -- Data valide

      -- Extensions (in option, not supported here)
      --core_mode   : out std_logic_vector(1 downto 0);       -- Processor Mode (Kernel, User, etc..)
      --irq_in      : in  std_logic;                          -- Signal IRQ in
      --except_in   : in  std_logic;                          -- For exceptions (bad address, bad mode, etc...)
    );
	end component;
	signal	if_adr			: Std_Logic_Vector(31 downto 0) ;
	signal	if_adr_valid	: Std_Logic;

	signal	ic_inst			: Std_Logic_Vector(31 downto 0) ;
	signal	ic_stall			: Std_Logic;
   
	signal	mem_adr			: Std_Logic_Vector(31 downto 0);
	signal	mem_stw			: Std_Logic;
	signal	mem_sth			: Std_Logic;
	signal	mem_stb			: Std_Logic;
	signal	mem_load			: Std_Logic;

	signal	mem_data			: Std_Logic_Vector(31 downto 0);
	signal	dc_data			: Std_Logic_Vector(31 downto 0);
	signal	dc_stall			: Std_Logic;

	signal	ck					: Std_Logic;
	signal	reset_n			: Std_Logic;
	signal	vdd				: bit := '1';
	signal	vss				: bit := '0';

	signal	GoodAdr			: Std_Logic_Vector(31 downto 0) ;
	signal	BadAdr			: Std_Logic_Vector(31 downto 0) ;

  signal dvalid : Std_Logic;
  signal ivalid : Std_Logic;
  signal mem_size : std_logic_vector( 2 downto 0);
  signal mem_req : Std_Logic;
  signal mem_w_not_r : Std_Logic;



  constant latency_icache : natural := 0;
  constant write_latency : std_logic_vector(3 downto 0) := X"0"; -- + stall for x cycle
  constant read_latency : std_logic_vector(3 downto 0) := X"0"; -- + stall for x cycle
  constant do_random : std_logic := '0';

	begin
	--  Component instantiation.

	icache_i : icache
  generic map(
    latency => latency_icache,
    random_latency => do_random
  )
	port map (
          ck => ck,
          if_adr			=> if_adr,
					if_adr_valid	=> if_adr_valid,
					ic_inst			=> ic_inst,
					ic_stall			=> ic_stall);

	dcache_i : dcache
  generic map(
    write_latency =>  write_latency,
    read_latency => read_latency,
    random_latency => do_random
  )
	port map (	mem_adr	=> mem_adr,
					mem_stw	=> mem_stw,
					mem_sth	=> mem_sth,
					mem_stb	=> mem_stb,
					mem_load	=> mem_load,

					mem_data	=> mem_data,
					dc_data	=> dc_data,
					dc_stall	=> dc_stall,
					ck			=> ck);
  core_i : riscV_Core
  port map( -- Global Interface
      ck  => ck,
      reset_n => reset_n,
      VDD => vdd,
      VSS => vss,

      -- Instruction Cache Interface
      inst_adr => if_adr,
      inst_req => if_adr_valid,
      inst_in  => ic_inst,
      inst_valid => ivalid,

      -- Data Cache Interface
      data_adr => mem_adr,
      data_load_w => mem_load,
      data_store_b => mem_stb,
      data_store_h => mem_sth,
      data_store_w => mem_stw,
      data_out => mem_data,
      data_in => dc_data,
      data_valid => dvalid

      -- Extensions (in option, not supported here)
      --core_mode   : out std_logic_vector(1 downto 0);       -- Processor Mode (Kernel, User, etc..)
      --irq_in      : in  std_logic;                          -- Signal IRQ in
      --except_in   : in  std_logic;                          -- For exceptions (bad address, bad mode, etc...)
    );
    ivalid <= not ic_stall;
    dvalid <= not dc_stall;


process
  variable iadr : signed(31 downto 0);
begin
	GoodAdr <= std_logic_vector(TO_SIGNED(mem_goodadr, 32));
	BadAdr <= std_logic_vector(TO_SIGNED(mem_badadr, 32));

	reset_n <= '0';
	ck <= '0';
	wait for 1 ns;
	ck <= '1';
	wait for 1 ns;
	reset_n <= '1';
	while not (if_adr_valid = '1' and (if_adr = GoodAdr or if_adr = BadAdr)) loop
		ck <= '0';
		wait for 1 ns;
		ck <= '1';
		wait for 1 ns;
	end loop;

	iadr := signed(if_adr);
  if if_adr = GoodAdr then
    report "GOOD!!!" severity note;
    good_end(TO_INTEGER(iadr));
    --good_end();
  end if;

  if if_adr = BadAdr then
    report "BAD!!!" severity note;
    bad_end(TO_INTEGER(iadr));
    --bad_end();
  end if;

	assert false report "end of test" severity note;

wait;
end process;

end riscV_SoftSIMU;
