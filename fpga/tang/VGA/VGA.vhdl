-- Author : azad(adrian) <adrian <AT> azad.eu.org>
-- Date : 23/07/2019

LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
use IEEE.STD_LOGIC_1164.ALL;


-- Default for 800*600; timing found on http://tinyvga.com/vga-timing/800x600@60Hz
entity VGA IS
	generic( h_pixel : integer := 800;
  		 h_front_porch : integer :=  40;
  		 h_sync_pulse : integer := 128;
  		 h_back_porch : integer :=  88;
  		 h_total : integer := 1056;

  		 v_pixel : integer :=  600 ;
  		 v_front_porch : integer := 1 ;
  		 v_sync_pulse  : integer := 4 ;
  		 v_back_porch  : integer := 23 ;
  		 v_total : integer := 628 );
	PORT (clk_pixel	: IN	STD_LOGIC; -- clock pixel
		reset	: IN	STD_LOGIC;
		R,G,B : in STD_LOGIC;
		h_current_pixel, v_current_pixel : out integer;
		Rout, Gout, Bout, H, V : out std_logic);
END VGA;

architecture behavioural of VGA is

  signal v_counter, h_counter : integer;
  
  

begin

  -- Must be black when not pixel display => https://electronics.stackexchange.com/questions/221536/vga-driver-not-working
	Rout <= R when (h_counter < h_pixel) and (v_counter < v_pixel) and (reset = '1') else '0' ;
	Gout <= G when (h_counter < h_pixel) and (v_counter < v_pixel) and (reset = '1') else '0' ;
	Bout <= B when (h_counter < h_pixel) and (v_counter < v_pixel) and (reset = '1') else '0' ;


counter_sync : process(clk_pixel,reset)
begin
  if reset = '0' then
    h_counter <= 0;
    v_counter <= 0;
  elsif rising_edge(clk_pixel) then
    if h_counter = h_total then
      h_counter <= 0;
      if v_counter = v_total then
        v_counter <= 0;
      else
        v_counter <= v_counter + 1;
      end if;
    else
      h_counter <= h_counter + 1;
    end if;
  end if;
end process;

h_current_pixel <= h_counter when h_counter < h_pixel else 0;
H <= '1' when (h_counter >= h_pixel + h_front_porch) and (h_counter < h_pixel + h_front_porch + h_sync_pulse) else '0';

v_current_pixel <= v_counter when v_counter < v_pixel else 0;
V <= '1' when (v_counter >= v_pixel + v_front_porch) and (v_counter < v_pixel + v_front_porch + v_sync_pulse) else '0';

end architecture;
