library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.all;

entity r5_fpga is
port( -- Global Interface
      ck     : in std_logic;                               -- Core Clock
      reset_n   : in std_logic;                               -- Global synchrone Reset
      VGA_H, VGA_V, VGA_R, VGA_G, VGA_B : out std_logic
      );
end r5_fpga;

architecture Behavioral of r5_fpga is
component CLK_40
	PORT ( refclk	: IN	STD_LOGIC;
		extlock	: OUT	STD_LOGIC;
		clk0_out	: OUT	STD_LOGIC);
END component;
	signal clk_pixel, extlock : std_logic;

component VGA IS
	generic( h_pixel : integer := 800;
  		 h_front_porch : integer :=  40;
  		 h_sync_pulse : integer := 128;
  		 h_back_porch : integer :=  88;
  		 h_total : integer := 1056;

  		 v_pixel : integer :=  600 ;
  		 v_front_porch : integer := 1 ;
  		 v_sync_pulse  : integer := 4 ;
  		 v_back_porch  : integer := 23 ;
  		 v_total : integer := 628 );
	PORT (clk_pixel	: IN	STD_LOGIC; -- clock pixel
		reset	: IN	STD_LOGIC;
		R,G,B : in STD_LOGIC;
		h_current_pixel, v_current_pixel : out integer;
		Rout, Gout, Bout, H, V : out std_logic);
END component;
  signal R,G,B : std_logic;
  signal h_current_pixel, v_current_pixel : integer;

  component riscV_Core
  port( -- Global Interface
      ck     : in std_logic;                               -- Core Clock
      reset_n   : in std_logic;                               -- Global Asynchrone Reset
      VDD     : in bit;                                     -- Power Supply VDD Voltage
      VSS     : in bit;                                     -- Power Supply VSS Voltage
      -- Instruction Cache Interface
      inst_adr    : out std_logic_vector(31 downto 0);      -- Instruction address
      inst_req    : out std_logic;                          -- Instruction request
      inst_in     : in  std_logic_vector(31 downto 0);      -- Instruction given by ICache
      inst_valid  : in  std_logic;                          -- Instruction valide

      -- Data Cache Interface
      data_adr    : out std_logic_vector(31 downto 0);      -- Data address
      data_load_w : out std_logic;
      data_store_b : out std_logic;
      data_store_h : out std_logic;
      data_store_w : out std_logic;
      data_out    : out std_logic_vector(31 downto 0);      -- Data out (Store)
      data_in     : in  std_logic_vector(31 downto 0);      -- Data given by DCache
      data_valid  : in  std_logic                           -- Data valide

    );
	end component;
	signal	if_adr			: Std_Logic_Vector(31 downto 0) ;

	signal	ic_inst			: Std_Logic_Vector(31 downto 0) ;
	signal	ic_stall			: Std_Logic;
   
	signal	mem_adr			: Std_Logic_Vector(31 downto 0);

	signal	mem_data			: Std_Logic_Vector(31 downto 0);
	signal	dc_data			: Std_Logic_Vector(31 downto 0);
	signal	dc_stall			: Std_Logic;

  signal data_load_w,data_store_b,data_store_h,data_store_w, inst_req : std_logic;
  signal ivalid, dvalid : std_logic;
	signal vdd, vss : bit;
  signal vga_val : std_logic_vector(2 downto 0);
	
begin

CLK_40_ins : CLK_40
	PORT map ( refclk => ck,
			   extlock => extlock,
			   clk0_out	=> clk_pixel);

VGA_ins : VGA
	PORT map (clk_pixel => clk_pixel,
    reset => reset_n,
    R => R,
    G => G,
    B => B,
    h_current_pixel => h_current_pixel,
    v_current_pixel => v_current_pixel,
    Rout => VGA_R,
    Gout => VGA_G,
    Bout => VGA_B,
    H => VGA_H,
    V => VGA_V);

R <= vga_val(1);
G <= vga_val(0);
B <= vga_val(2);


  core_i : riscV_Core
  port map( -- Global Interface
      ck  => ck,
      reset_n => reset_n,
      vdd => vdd,
      vss => vss,

      -- Instruction Cache Interface
      inst_adr => if_adr,
      inst_req => inst_req,
      inst_in  => ic_inst,
      inst_valid => ivalid,

      -- Data Cache Interface
      data_adr => mem_adr,
      data_load_w => data_load_w,
      data_store_b => data_store_b,
      data_store_h => data_store_h,
      data_store_w => data_store_w,
      data_out => mem_data,
      data_valid => dvalid,
      data_in => dc_data
    );
    
  vdd <= '1';
  vss <= '0';
 process(ck)
 begin
 	if rising_edge(ck) then
 		if reset_n = '0' then
      vga_val <= "000";
 		else
      if data_store_b = '1' then --and mem_adr = X"08000010" then
        vga_val <= mem_data(2 downto 0);
      else
        vga_val <= vga_val;
      end if;
 		end if;
  end if;
end process;

ivalid <= '1' when if_adr = X"08000000" or if_adr = X"08000004" or if_adr = X"08000008" or if_adr = X"0800000C" or if_adr = X"08000010" or if_adr = X"08000014" or if_adr = X"08000018" or if_adr = X"0800001C" or if_adr = X"08000020" or if_adr = X"08000024" else '0';

dvalid <= '1' when data_store_b = '1' else '0';

ic_inst <= X"00010b97" when if_adr = X"08000000" else -- auipc s7, 16 -- load @ for vga, but dont care
           X"004af3b7" when if_adr = X"08000004" else -- lui  t2,0x4af --change value here to change time between change color
           X"00000333" when if_adr = X"08000008" else -- add  t1,zero,zero
           X"000002b3" when if_adr = X"0800000C" else -- x :add  t0,zero,zero
           X"00128293" when if_adr = X"08000010" else -- w : addi t0,t0,1
           X"fe729ee3" when if_adr = X"08000014" else -- bne  t0,t2,c <w>
           X"00130313" when if_adr = X"08000018" else -- addi t1,t1,1
           X"006b8023" when if_adr = X"0800001C" else -- sb t1,0(s7)
           X"fedff06f" when if_adr = X"08000020" else -- j  8 <x>
           X"00000000";

dc_data <= X"00000000";


end Behavioral;
