nb_test=0
nb_test_ok=0
nb_test_ko=0
total_cpi=0

for file in $(find riscv-tests/isa/bin/ -type f ! -name "*.*" -name "rv32ui-p-*")
do
    test_name=$(basename $file)
    if [[ -f $file ]] && ! [[ $test_name =~ .*fence.* ]]; then
        wave_file=wave/${test_name}-post-synthesis.ghw
        echo "../Simu-post-synthesis/riscv_softsimu_post_synthesis $file --wave=${wave_file}"
        ../Simu-post-synthesis/riscv_softsimu_post_synthesis $file --wave=${wave_file} &> /dev/null
        exit_code=$?
        nb_test=$(( nb_test + 1))
        if [ $exit_code -eq 0 ]
        then
            echo -e "$test_name is \033[32mOK\033[0m"
            nb_test_ok=$(( nb_test_ok + 1))
            rm ${wave_file}
        else
            echo -e "$test_name is \033[31mKO\033[0m"
            nb_test_ko=$(( nb_test_ko + 1))
        fi
    fi
done
echo -e "NB test $nb_test ( $nb_test_ok PASS, $nb_test_ko FAIL)"


