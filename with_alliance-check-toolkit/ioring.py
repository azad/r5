from helpers import l, u, n
chip ={
    'pads.ioPadGauge' : 'LEF.ioSite_P',
    'pads.instances' : [
#      ["ck_ins", "pad_ck", "ck"],
      ["reset_n_ins", "pad_reset_n", "reset_n"],
      ["inst_adr_0_ins", "pad_inst_adr_0", "inst_adr(0)"],
      ["inst_adr_1_ins", "pad_inst_adr_1", "inst_adr(1)"],
      ["inst_adr_2_ins", "pad_inst_adr_2", "inst_adr(2)"],
      ["inst_adr_3_ins", "pad_inst_adr_3", "inst_adr(3)"],
      ["inst_adr_4_ins", "pad_inst_adr_4", "inst_adr(4)"],
      ["inst_adr_5_ins", "pad_inst_adr_5", "inst_adr(5)"],
      ["inst_adr_6_ins", "pad_inst_adr_6", "inst_adr(6)"],
      ["inst_adr_7_ins", "pad_inst_adr_7", "inst_adr(7)"],
      ["inst_adr_8_ins", "pad_inst_adr_8", "inst_adr(8)"],
      ["inst_adr_9_ins", "pad_inst_adr_9", "inst_adr(9)"],
      ["inst_adr_10_ins", "pad_inst_adr_10", "inst_adr(10)"],
      ["inst_adr_11_ins", "pad_inst_adr_11", "inst_adr(11)"],
      ["inst_adr_12_ins", "pad_inst_adr_12", "inst_adr(12)"],
      ["inst_adr_13_ins", "pad_inst_adr_13", "inst_adr(13)"],
      ["inst_adr_14_ins", "pad_inst_adr_14", "inst_adr(14)"],
      ["inst_adr_15_ins", "pad_inst_adr_15", "inst_adr(15)"],
      ["inst_adr_16_ins", "pad_inst_adr_16", "inst_adr(16)"],
      ["inst_adr_17_ins", "pad_inst_adr_17", "inst_adr(17)"],
      ["inst_adr_18_ins", "pad_inst_adr_18", "inst_adr(18)"],
      ["inst_adr_19_ins", "pad_inst_adr_19", "inst_adr(19)"],
      ["inst_adr_20_ins", "pad_inst_adr_20", "inst_adr(20)"],
      ["inst_adr_21_ins", "pad_inst_adr_21", "inst_adr(21)"],
      ["inst_adr_22_ins", "pad_inst_adr_22", "inst_adr(22)"],
      ["inst_adr_23_ins", "pad_inst_adr_23", "inst_adr(23)"],
      ["inst_adr_24_ins", "pad_inst_adr_24", "inst_adr(24)"],
      ["inst_adr_25_ins", "pad_inst_adr_25", "inst_adr(25)"],
      ["inst_adr_26_ins", "pad_inst_adr_26", "inst_adr(26)"],
      ["inst_adr_27_ins", "pad_inst_adr_27", "inst_adr(27)"],
      ["inst_adr_28_ins", "pad_inst_adr_28", "inst_adr(28)"],
      ["inst_adr_29_ins", "pad_inst_adr_29", "inst_adr(29)"],
      ["inst_adr_30_ins", "pad_inst_adr_30", "inst_adr(30)"],
      ["inst_adr_31_ins", "pad_inst_adr_31", "inst_adr(31)"],
      ["inst_req_ins", "pad_inst_req", "inst_req"],
      ["inst_in_0_ins", "pad_inst_in_0", "inst_in(0)"],
      ["inst_in_1_ins", "pad_inst_in_1", "inst_in(1)"],
      ["inst_in_2_ins", "pad_inst_in_2", "inst_in(2)"],
      ["inst_in_3_ins", "pad_inst_in_3", "inst_in(3)"],
      ["inst_in_4_ins", "pad_inst_in_4", "inst_in(4)"],
      ["inst_in_5_ins", "pad_inst_in_5", "inst_in(5)"],
      ["inst_in_6_ins", "pad_inst_in_6", "inst_in(6)"],
      ["inst_in_7_ins", "pad_inst_in_7", "inst_in(7)"],
      ["inst_in_8_ins", "pad_inst_in_8", "inst_in(8)"],
      ["inst_in_9_ins", "pad_inst_in_9", "inst_in(9)"],
      ["inst_in_10_ins", "pad_inst_in_10", "inst_in(10)"],
      ["inst_in_11_ins", "pad_inst_in_11", "inst_in(11)"],
      ["inst_in_12_ins", "pad_inst_in_12", "inst_in(12)"],
      ["inst_in_13_ins", "pad_inst_in_13", "inst_in(13)"],
      ["inst_in_14_ins", "pad_inst_in_14", "inst_in(14)"],
      ["inst_in_15_ins", "pad_inst_in_15", "inst_in(15)"],
      ["inst_in_16_ins", "pad_inst_in_16", "inst_in(16)"],
      ["inst_in_17_ins", "pad_inst_in_17", "inst_in(17)"],
      ["inst_in_18_ins", "pad_inst_in_18", "inst_in(18)"],
      ["inst_in_19_ins", "pad_inst_in_19", "inst_in(19)"],
      ["inst_in_20_ins", "pad_inst_in_20", "inst_in(20)"],
      ["inst_in_21_ins", "pad_inst_in_21", "inst_in(21)"],
      ["inst_in_22_ins", "pad_inst_in_22", "inst_in(22)"],
      ["inst_in_23_ins", "pad_inst_in_23", "inst_in(23)"],
      ["inst_in_24_ins", "pad_inst_in_24", "inst_in(24)"],
      ["inst_in_25_ins", "pad_inst_in_25", "inst_in(25)"],
      ["inst_in_26_ins", "pad_inst_in_26", "inst_in(26)"],
      ["inst_in_27_ins", "pad_inst_in_27", "inst_in(27)"],
      ["inst_in_28_ins", "pad_inst_in_28", "inst_in(28)"],
      ["inst_in_29_ins", "pad_inst_in_29", "inst_in(29)"],
      ["inst_in_30_ins", "pad_inst_in_30", "inst_in(30)"],
      ["inst_in_31_ins", "pad_inst_in_31", "inst_in(31)"],
      ["inst_valid_ins", "pad_inst_valid", "inst_valid"],
      ["data_adr_0_ins", "pad_data_adr_0", "data_adr(0)"],
      ["data_adr_1_ins", "pad_data_adr_1", "data_adr(1)"],
      ["data_adr_2_ins", "pad_data_adr_2", "data_adr(2)"],
      ["data_adr_3_ins", "pad_data_adr_3", "data_adr(3)"],
      ["data_adr_4_ins", "pad_data_adr_4", "data_adr(4)"],
      ["data_adr_5_ins", "pad_data_adr_5", "data_adr(5)"],
      ["data_adr_6_ins", "pad_data_adr_6", "data_adr(6)"],
      ["data_adr_7_ins", "pad_data_adr_7", "data_adr(7)"],
      ["data_adr_8_ins", "pad_data_adr_8", "data_adr(8)"],
      ["data_adr_9_ins", "pad_data_adr_9", "data_adr(9)"],
      ["data_adr_10_ins", "pad_data_adr_10", "data_adr(10)"],
      ["data_adr_11_ins", "pad_data_adr_11", "data_adr(11)"],
      ["data_adr_12_ins", "pad_data_adr_12", "data_adr(12)"],
      ["data_adr_13_ins", "pad_data_adr_13", "data_adr(13)"],
      ["data_adr_14_ins", "pad_data_adr_14", "data_adr(14)"],
      ["data_adr_15_ins", "pad_data_adr_15", "data_adr(15)"],
      ["data_adr_16_ins", "pad_data_adr_16", "data_adr(16)"],
      ["data_adr_17_ins", "pad_data_adr_17", "data_adr(17)"],
      ["data_adr_18_ins", "pad_data_adr_18", "data_adr(18)"],
      ["data_adr_19_ins", "pad_data_adr_19", "data_adr(19)"],
      ["data_adr_20_ins", "pad_data_adr_20", "data_adr(20)"],
      ["data_adr_21_ins", "pad_data_adr_21", "data_adr(21)"],
      ["data_adr_22_ins", "pad_data_adr_22", "data_adr(22)"],
      ["data_adr_23_ins", "pad_data_adr_23", "data_adr(23)"],
      ["data_adr_24_ins", "pad_data_adr_24", "data_adr(24)"],
      ["data_adr_25_ins", "pad_data_adr_25", "data_adr(25)"],
      ["data_adr_26_ins", "pad_data_adr_26", "data_adr(26)"],
      ["data_adr_27_ins", "pad_data_adr_27", "data_adr(27)"],
      ["data_adr_28_ins", "pad_data_adr_28", "data_adr(28)"],
      ["data_adr_29_ins", "pad_data_adr_29", "data_adr(29)"],
      ["data_adr_30_ins", "pad_data_adr_30", "data_adr(30)"],
      ["data_adr_31_ins", "pad_data_adr_31", "data_adr(31)"],
      ["data_load_w_ins", "pad_data_load_w", "data_load_w"],
      ["data_store_b_ins", "pad_data_store_b", "data_store_b"],
      ["data_store_h_ins", "pad_data_store_h", "data_store_h"],
      ["data_store_w_ins", "pad_data_store_w", "data_store_w"],
      ["data_0_ins", "pad_data_0", "data_in(0)", "data_out(0)", "data_load_w" ],
      ["data_1_ins", "pad_data_1", "data_in(1)", "data_out(1)", "data_load_w" ],
      ["data_2_ins", "pad_data_2", "data_in(2)", "data_out(2)", "data_load_w" ],
      ["data_3_ins", "pad_data_3", "data_in(3)", "data_out(3)", "data_load_w" ],
      ["data_4_ins", "pad_data_4", "data_in(4)", "data_out(4)", "data_load_w" ],
      ["data_5_ins", "pad_data_5", "data_in(5)", "data_out(5)", "data_load_w" ],
      ["data_6_ins", "pad_data_6", "data_in(6)", "data_out(6)", "data_load_w" ],
      ["data_7_ins", "pad_data_7", "data_in(7)", "data_out(7)", "data_load_w" ],
      ["data_8_ins", "pad_data_8", "data_in(8)", "data_out(8)", "data_load_w" ],
      ["data_9_ins", "pad_data_9", "data_in(9)", "data_out(9)", "data_load_w" ],
      ["data_10_ins", "pad_data_10", "data_in(10)", "data_out(10)", "data_load_w" ],
      ["data_11_ins", "pad_data_11", "data_in(11)", "data_out(11)", "data_load_w" ],
      ["data_12_ins", "pad_data_12", "data_in(12)", "data_out(12)", "data_load_w" ],
      ["data_13_ins", "pad_data_13", "data_in(13)", "data_out(13)", "data_load_w" ],
      ["data_14_ins", "pad_data_14", "data_in(14)", "data_out(14)", "data_load_w" ],
      ["data_15_ins", "pad_data_15", "data_in(15)", "data_out(15)", "data_load_w" ],
      ["data_16_ins", "pad_data_16", "data_in(16)", "data_out(16)", "data_load_w" ],
      ["data_17_ins", "pad_data_17", "data_in(17)", "data_out(17)", "data_load_w" ],
      ["data_18_ins", "pad_data_18", "data_in(18)", "data_out(18)", "data_load_w" ],
      ["data_19_ins", "pad_data_19", "data_in(19)", "data_out(19)", "data_load_w" ],
      ["data_20_ins", "pad_data_20", "data_in(20)", "data_out(20)", "data_load_w" ],
      ["data_21_ins", "pad_data_21", "data_in(21)", "data_out(21)", "data_load_w" ],
      ["data_22_ins", "pad_data_22", "data_in(22)", "data_out(22)", "data_load_w" ],
      ["data_23_ins", "pad_data_23", "data_in(23)", "data_out(23)", "data_load_w" ],
      ["data_24_ins", "pad_data_24", "data_in(24)", "data_out(24)", "data_load_w" ],
      ["data_25_ins", "pad_data_25", "data_in(25)", "data_out(25)", "data_load_w" ],
      ["data_26_ins", "pad_data_26", "data_in(26)", "data_out(26)", "data_load_w" ],
      ["data_27_ins", "pad_data_27", "data_in(27)", "data_out(27)", "data_load_w" ],
      ["data_28_ins", "pad_data_28", "data_in(28)", "data_out(28)", "data_load_w" ],
      ["data_29_ins", "pad_data_29", "data_in(29)", "data_out(29)", "data_load_w" ],
      ["data_30_ins", "pad_data_30", "data_in(30)", "data_out(30)", "data_load_w" ],
      ["data_31_ins", "pad_data_31", "data_in(31)", "data_out(31)", "data_load_w" ],
      ["data_valid_ins", "pad_data_valid", "data_valid"],
      ["scin_ins", "pad_scin", "scin"],
      ["scout_ins", "pad_scout", "scout"],
      ["test_ins", "pad_test", "test"],
    ],
    'pads.north' : [
              "reset_n_ins"
            , "inst_adr_0_ins"
            , "inst_adr_1_ins"
            , "inst_adr_2_ins"
            , "inst_adr_3_ins"
            , "inst_adr_4_ins"
            , "inst_adr_5_ins"
            , "inst_adr_6_ins"
            , "inst_adr_7_ins"
            , "inst_adr_8_ins"
            , "inst_adr_9_ins"
            , "inst_adr_10_ins"
            , "inst_adr_11_ins"
            , "inst_adr_12_ins"
            , "inst_adr_13_ins"
            , "inst_adr_14_ins"
            , "inst_adr_15_ins"
            , "ck_0"
            , "inst_adr_16_ins"
            , "inst_adr_17_ins"
            , "inst_adr_18_ins"
            , "inst_adr_19_ins"
            , "inst_adr_20_ins"
            , "inst_adr_21_ins"
            , "inst_adr_22_ins"
            , "inst_adr_23_ins"
            , "inst_adr_24_ins"
            , "inst_adr_25_ins"
            , "inst_adr_26_ins"
            , "inst_adr_27_ins"
            , "inst_adr_28_ins"
            , "inst_adr_29_ins"
            , "inst_adr_30_ins"
            , "inst_adr_31_ins"
            , "inst_req_ins"
            , "scin_ins"
            , "test_ins"
        ],
    'pads.west' : [
             "inst_in_0_ins"
            , "inst_in_1_ins"
            , "inst_in_2_ins"
            , "inst_in_3_ins"
            , "inst_in_4_ins"
            , "inst_in_5_ins"
            , "inst_in_6_ins"
            , "inst_in_7_ins"
            , "inst_in_8_ins"
            , "inst_in_9_ins"
            , "inst_in_10_ins"
            , "inst_in_11_ins"
            , "inst_in_12_ins"
            , "inst_in_13_ins"
            , "inst_in_14_ins"
            , "inst_in_15_ins"
            , "inst_in_16_ins"
            , "vdd_i_0"
            , "vss_i_0"
            , "inst_in_17_ins"
            , "inst_in_18_ins"
            , "inst_in_19_ins"
            , "inst_in_20_ins"
            , "inst_in_21_ins"
            , "inst_in_22_ins"
            , "inst_in_23_ins"
            , "inst_in_24_ins"
            , "inst_in_25_ins"
            , "inst_in_26_ins"
            , "inst_in_27_ins"
            , "inst_in_28_ins"
            , "inst_in_29_ins"
            , "inst_in_30_ins"
            , "inst_in_31_ins"
            , "inst_valid_ins"
        ],
    'pads.south' : [
             "data_adr_0_ins"
            , "data_adr_1_ins"
            , "data_adr_2_ins"
            , "data_adr_3_ins"
            , "data_adr_4_ins"
            , "data_adr_5_ins"
            , "data_adr_6_ins"
            , "data_adr_7_ins"
            , "data_adr_8_ins"
            , "data_adr_9_ins"
            , "data_adr_10_ins"
            , "data_adr_11_ins"
            , "data_adr_12_ins"
            , "data_adr_13_ins"
            , "data_adr_14_ins"
            , "data_adr_15_ins"
            , "data_adr_16_ins"
            , "data_adr_17_ins"
            , "data_adr_18_ins"
            , "data_adr_19_ins"
            , "data_adr_20_ins"
            , "data_adr_21_ins"
            , "data_adr_22_ins"
            , "data_adr_23_ins"
            , "data_adr_24_ins"
            , "data_adr_25_ins"
            , "data_adr_26_ins"
            , "data_adr_27_ins"
            , "data_adr_28_ins"
            , "data_adr_29_ins"
            , "data_adr_30_ins"
            , "data_adr_31_ins"
            , "data_load_w_ins"
            , "data_store_b_ins"
            , "data_store_h_ins"
            , "data_store_w_ins"
            , "data_0_ins"
        ],
    'pads.east' : [
              "scout_ins"
            , "data_1_ins"
            , "data_2_ins"
            , "data_3_ins"
            , "data_4_ins"
            , "data_5_ins"
            , "data_6_ins"
            , "data_7_ins"
            , "data_8_ins"
            , "data_9_ins"
            , "data_10_ins"
            , "data_11_ins"
            , "data_12_ins"
            , "data_13_ins"
            , "data_14_ins"
            , "data_15_ins"
            , "data_16_ins"
            , "vdd_o_0"
            , "vss_o_0"
            , "data_17_ins"
            , "data_18_ins"
            , "data_19_ins"
            , "data_20_ins"
            , "data_21_ins"
            , "data_22_ins"
            , "data_23_ins"
            , "data_24_ins"
            , "data_25_ins"
            , "data_26_ins"
            , "data_27_ins"
            , "data_28_ins"
            , "data_29_ins"
            , "data_30_ins"
            , "data_31_ins"
            , "data_valid_ins"
        ],
   #'chip.size' : (u( 4800.0),u( 4800.0)),
    'chip.size' : (u( 4850.0),u( 4850.0)),
    'core.size':  (l(11000  ),l(11000  )),
    'chip.clockTree': True,
}



