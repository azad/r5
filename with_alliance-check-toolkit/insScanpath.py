#!/usr/bin/env python
# -*- coding: utf-8 -*-

try:
  import sys
  import traceback
  import os.path
  import shutil
  import subprocess
  import optparse
  import math
  import Cfg
  import Hurricane
  from   Hurricane import DataBase
  from   Hurricane import DbU
  from   Hurricane import Transformation
  from   Hurricane import Box
  from   Hurricane import UpdateSession
  from   Hurricane import Breakpoint
  from   Hurricane import Net
  from   Hurricane import NetExternalComponents
  from   Hurricane import BasicLayer
  from   Hurricane import ContactLayer
  from   Hurricane import ViaLayer
  from   Hurricane import RegularLayer
  from   Hurricane import TransistorLayer
  from   Hurricane import DiffusionLayer
  from   Hurricane import Cell
  from   Hurricane import Instance
  from   Hurricane import Net
  from   Hurricane import Contact
  from   Hurricane import Horizontal
  from   Hurricane import Vertical
  import Viewer
  import CRL
  from   CRL import RoutingLayerGauge
  import helpers
  from   helpers import trace
  from   helpers.io import ErrorMessage
except ImportError, e:
  serror = str(e)
  if serror.startswith('No module named'):
    module = serror.split()[-1]
    print '[ERROR] The <%s> python module or symbol cannot be loaded.' % module
    print '        Please check the integrity of the <coriolis> package.'
  if str(e).find('cannot open shared object file'):
    library = serror.split(':')[0]
    print '[ERROR] The <%s> shared library cannot be loaded.' % library
    print '        Under RHEL 6, you must be under devtoolset-2.'
    print '        (scl enable devtoolset-2 bash)'
  sys.exit(1)
except Exception, e:
  print '[ERROR] A strange exception occurred while loading the basic Coriolis/Python'
  print '        modules. Something may be wrong at Python/C API level.\n'
  print '        %s' % e
  sys.exit(2)


framework = CRL.AllianceFramework.get()

def getDriverInstanceFromNet(net):
    driverInstance = None
    for plug in net.getPlugs():
        if not plug.isConnected():
            pass
        if plug.getInstance().getMasterCell().getNet(plug.getMasterNet().getName()).getDirection() == Net.Direction.OUT:
            if driverInstance != None:
                print ErrorMessage( 1, '<%s> have multiple driver.' % net.getName() )
            driverInstance = plug.getInstance()
    return driverInstance

def ScanPath(cell, configScanPath, f_signal, signal_prefix):
    # Insert port in, out, test of scanpath
    netIn=Net.create(cell,"scin");
    netIn.setDirection(Net.Direction.IN)
    netIn.setExternal(True)

    netTest=Net.create(cell,"test");
    netTest.setDirection(Net.Direction.IN)
    netTest.setExternal(True)

    netOut=Net.create(cell,"scout");
    netOut.setDirection(Net.Direction.OUT)
    netOut.setExternal(True)

    # /!\ cell entry must be same across all cell lib (sff3 entry i0, i1, i3 and cmd0, cmd1 should drive same result)
    Cell_sff2_x4 = framework.getCell( "sff2_x4" , CRL.Catalog.State.Views )
    Cell_sff3_x4 = framework.getCell( "sff3_x4" , CRL.Catalog.State.Views )
    Cell_mx2_x2 = framework.getCell( "mx2_x2" , CRL.Catalog.State.Views )
    netCk = cell.getNet("ck")
    numero_instance = 0
    previousNetInScanPath = netIn
    for signal_name in configScanPath:
        if type(signal_name) == type(""): # It's a standard cell to insert
            signal_name_net = cell.getNet(signal_name)
            if signal_name_net == None :
                raise ErrorMessage( 1, 'Signal <%s> not found .' % signal_name )
            instance_driver = getDriverInstanceFromNet(signal_name_net)
            if instance_driver == None :
                raise ErrorMessage( 1, 'Net %s has no driver.' % signa_name)
            if instance_driver.getMasterCell().getName() not in ["sff1_x4", "sff2_x4"]:
                raise ErrorMessage( 1, 'Try to insert on no sff1 cell (%s).' % signal_prefix + "/" + signal_name_net.getName())
            f_signal.write(signal_prefix + "/" + signal_name_net.getName() + "\n")

            if instance_driver.getMasterCell().getName() == "sff1_x4":
                # Add sff2 instance
                instanceSff2 = Instance.create(cell,"sff2_x4_"+str(numero_instance),Cell_sff2_x4)
                numero_instance+=1

                #find netIO
                masternet_i = instance_driver.getPlug(instance_driver.getMasterCell().getNet("i"))
                netI0 = masternet_i.getNet()
                if netI0 == None:
                    raise ErrorMessage( 1, 'Not net driver found for instance <%s>.' % instance_driver.getName())

                # Connected i1 and cmd
                instanceSff2.getPlug(Cell_sff2_x4.getNet("ck")).setNet(netCk)
                instanceSff2.getPlug(Cell_sff2_x4.getNet("i0")).setNet(netI0)
                instanceSff2.getPlug(Cell_sff2_x4.getNet("i1")).setNet(previousNetInScanPath)
                instanceSff2.getPlug(Cell_sff2_x4.getNet("cmd")).setNet(netTest)
                instanceSff2.getPlug(Cell_sff2_x4.getNet("q")).setNet(signal_name_net)

                #Remove instance of sff1 cell
                instance_driver.destroy()
            elif instance_driver.getMasterCell().getName() == "sff2_x4": 
                # 2 solutions : mux (from current sff2) + sff2 (with mux result + scan value) or sff3 but require not test signal (high fan-out?)
                # Add sff3 instance
                instanceMux = Instance.create(cell,"mx2_x2_"+str(numero_instance),Cell_mx2_x2)
                numero_instance+=1

                #find netIO, I1, cmd
                masternet_i0 = instance_driver.getPlug(instance_driver.getMasterCell().getNet("i0"))
                netI0 = masternet_i0.getNet()
                if netI0 == None:
                    raise ErrorMessage( 1, 'Not net i0 driver found for instance <%s>.' % instance_driver.getName())

                masternet_i1 = instance_driver.getPlug(instance_driver.getMasterCell().getNet("i1"))
                netI1 = masternet_i1.getNet()
                if netI1 == None:
                    raise ErrorMessage( 1, 'Not net i1 driver found for instance <%s>.' % instance_driver.getName())

                masternet_cmd = instance_driver.getPlug(instance_driver.getMasterCell().getNet("cmd"))
                netcmd = masternet_cmd.getNet()
                if netcmd == None:
                    raise ErrorMessage( 1, 'Not net cmd driver found for instance <%s>.' % instance_driver.getName())
                # Create output net
                NetMux = Net.create(cell,"scanpath_mux_net_"+str(numero_instance))

                # Connect mux
                instanceMux.getPlug(Cell_mx2_x2.getNet("i0")).setNet(netI0)
                instanceMux.getPlug(Cell_mx2_x2.getNet("i1")).setNet(netI1)
                instanceMux.getPlug(Cell_mx2_x2.getNet("cmd")).setNet(netcmd)
                instanceMux.getPlug(Cell_mx2_x2.getNet("q")).setNet(NetMux)

                # Connect sff2
                instance_driver.getPlug(Cell_sff2_x4.getNet("ck")).setNet(netCk)
                instance_driver.getPlug(Cell_sff2_x4.getNet("i0")).setNet(NetMux)
                instance_driver.getPlug(Cell_sff2_x4.getNet("i1")).setNet(previousNetInScanPath)
                instance_driver.getPlug(Cell_sff2_x4.getNet("cmd")).setNet(netTest)

                instance_driver.getPlug(Cell_sff2_x4.getNet("q")).setNet(signal_name_net)

            else:
                raise ErrorMessage( 1, 'Something wrong... cell should be sff1 or sff2.')

            previousNetInScanPath = signal_name_net
            if numero_instance == len(configScanPath) : # if it is last scanpath insered then output is scout
                #Insert buffer, but in reallity it's just scout <= previous_scan_path
                Cell_buf = framework.getCell( "buf_x2" , CRL.Catalog.State.Views )
                instanceBufScout = Instance.create(cell,"buf_x2_scout",Cell_buf)
                instanceBufScout.getPlug(Cell_buf.getNet("i")).setNet(previousNetInScanPath)
                instanceBufScout.getPlug(Cell_buf.getNet("q")).setNet(netOut)



        else: # it's an insertion on instance
            if len(signal_name.keys()) != 1:
                raise ErrorMessage( 1, 'Error on format... {} should only have one element.')
            instance_name=list(signal_name.keys())[0]
            instance = cell.getInstance(instance_name)
            if instance == None:
                raise ErrorMessage( 1, 'Instance <%s> not found .' % instance_name )

            instanceMaster = instance.getMasterCell()
            masterCellName = instanceMaster.getName()
            ScanPath(instanceMaster, signal_name[instance_name], f_signal, signal_prefix + "/" + instance.getName() )
            numero_instance+=1

            # Connect new term (scin, scout,test)
            instance.getPlug(instanceMaster.getNet("scin")).setNet(previousNetInScanPath)
            instance.getPlug(instanceMaster.getNet("test")).setNet(netTest)

            if numero_instance == len(configScanPath) : # if it is last scanpath insered then output is scout
                previousNetInScanPath = netOut
            else:
                previousNetInScanPath = Net.create(cell, "scanpath_net_" + str(numero_instance))
            instance.getPlug(instanceMaster.getNet("scout")).setNet(previousNetInScanPath)


    cell.setName(cell.getName() + "_scan")
    framework.saveCell(cell, CRL.Catalog.State.Logical | CRL.Catalog.State.VstUseConcat)
    return 1

def insScanPathR5( cell, configScanPath, f_signal, signal_prefix ):
    UpdateSession.open()
    ScanPath( cell, configScanPath, f_signal, signal_prefix )

    # Insert an12 to block write to general purpose registers when test (insert or extract value)
    cell_rv_core = cell
    #TODO rename to cell

    netWEnableAndNotTest = Net.create(cell_rv_core,"w_enable_and_not_test")
    Cellan12_x1 = framework.getCell( "an12_x1" , CRL.Catalog.State.Views )
    netWEnable = cell_rv_core.getNet("w_enable");
    netTest = cell_rv_core.getNet("test")

    for instance in cell_rv_core.getInstances():
        if instance.getMasterCell().getName()  == "dec_decode_stage_scan":
            instance.getPlug(instance.getMasterCell().getNet("w_enable")).setNet(netWEnableAndNotTest)

    instanceWEnableAndNotTest = Instance.create(cell_rv_core,"an12_x1_WEnableAndNotTest",Cellan12_x1)
    instanceWEnableAndNotTest.getPlug(Cellan12_x1.getNet("i0")).setNet(netTest)
    instanceWEnableAndNotTest.getPlug(Cellan12_x1.getNet("i1")).setNet(netWEnable)
    instanceWEnableAndNotTest.getPlug(Cellan12_x1.getNet("q")).setNet(netWEnableAndNotTest)
    framework.saveCell(cell_rv_core, CRL.Catalog.State.Logical | CRL.Catalog.State.VstUseConcat)


    # Insert in dec_decode_stage an12 to block inval request for register when test (insert or extract value)
    dec_cell = None
    for instance in cell_rv_core.getInstances():
        if instance.getMasterCell().getName()  == "dec_decode_stage_scan":
            dec_cell=instance.getMasterCell()

    dec_netTest = dec_cell.getNet("test")
    dec_inval_req = dec_cell.getNet("inval_req")

    netInval_reqAndNotTest = Net.create(dec_cell,"inval_req_and_not_test")

    for instance in dec_cell.getInstances():
        if instance.getMasterCell().getName()  == "reg_reg_i":
            instance.getPlug(instance.getMasterCell().getNet("inval_req")).setNet(netInval_reqAndNotTest)

    instanceInval_reqAndNotTest = Instance.create(dec_cell,"an12_x1_Inval_reqAndNotTest",Cellan12_x1)
    instanceInval_reqAndNotTest.getPlug(Cellan12_x1.getNet("i0")).setNet(dec_netTest)
    instanceInval_reqAndNotTest.getPlug(Cellan12_x1.getNet("i1")).setNet(dec_inval_req)
    instanceInval_reqAndNotTest.getPlug(Cellan12_x1.getNet("q")).setNet(netInval_reqAndNotTest)
    framework.saveCell(dec_cell, CRL.Catalog.State.Logical | CRL.Catalog.State.VstUseConcat)

    """
    # Insert an12 to block memory request when test (insert or extract value)
    net_req_intern = cell.getNet("req")
    net_req_intern.setName("req_internal")
    net_req_intern.setExternal(False)

    net_req=Net.create(cell,"req");
    net_req.setDirection(Net.Direction.OUT)
    net_req.setExternal(True)

    netTest_req = cell.getNet("test")

    instanceReqAndNotTest = Instance.create(cell,"an12_x1_ReqAndNotTest",Cellan12_x1)
    instanceReqAndNotTest.getPlug(Cellan12_x1.getNet("i0")).setNet(netTest_req)
    instanceReqAndNotTest.getPlug(Cellan12_x1.getNet("i1")).setNet(net_req_intern)
    instanceReqAndNotTest.getPlug(Cellan12_x1.getNet("q")).setNet(net_req)
    """

    UpdateSession.close()

    framework.saveCell(cell, CRL.Catalog.State.Logical | CRL.Catalog.State.VstUseConcat)
    return 1

def loadScanPathConfiguration ( cell, netlistformat ):
    sys.path.append( os.getcwd() )

    confFile = cell.getName() + '_' + netlistformat + '_scanpath'
    if not os.path.isfile(confFile+'.py'):
      raise ErrorMessage( 1, 'ScanPath configuration file <%s.py> is missing.' % confFile )

    confModule = __import__( confFile, globals(), locals(), confFile )

    if not confModule.__dict__.has_key('scanpath'):
      ErrorMessage( 1, 'Module <%s> do not provides the chip variable, skipped.' \
                       % confFile )

    return confModule.__dict__['scanpath']

def ScriptMain ( **kw ):
  global framework

  helpers.staticInitialization( quiet=True )
 #helpers.setTraceLevel( 550 )

  scaledDir = framework.getAllianceLibrary(0).getPath()
  alibrary  = framework.getAllianceLibrary(1)

  sourceCell = None
  if kw.has_key('cell') and kw['cell']:
    sourceCell = kw['cell']
  else:
    print ErrorMessage( 1, 'No cell found.' )
    return 0

  netlistformat = None
  if kw.has_key('netlistformat') and kw['netlistformat']:
    netlistformat = kw['netlistformat']
  else:
    print ErrorMessage( 1, 'No netlistformat found.' )
    return 0

  prefix = ""
  if kw.has_key('prefix') and kw['prefix']:
    netlistformat = kw['prefix']


  if sourceCell:
    # Read config file
    configScanPath = loadScanPathConfiguration ( sourceCell, netlistformat )
    f_signal = open(sourceCell.getName() + "_signal.path","w")
    scanpath_ret = insScanPathR5(sourceCell, configScanPath, f_signal, "")
    f_signal.close()

    return scanpath_ret
  return 0


if __name__ == '__main__':
  parser = optparse.OptionParser()
  parser.add_option( '-c', '--cell', type='string',                      dest='cell'       , help='The name of the chip to build, whithout extension.')
  parser.add_option( '-f', '--netlistformat', type='string',                      dest='netlistformat'       , help='Format of netlist.')
  parser.add_option( '-v', '--verbose'            , action='store_true', dest='verbose'    , help='First level of verbosity.')
  parser.add_option( '-V', '--very-verbose'       , action='store_true', dest='veryVerbose', help='Second level of verbosity.')
  (options, args) = parser.parse_args()

  kw = {}
  if options.cell:
    kw['cell'] = framework.getCell( options.cell, CRL.Catalog.State.Views )
  if options.netlistformat:
    kw['netlistformat'] = options.netlistformat
  if options.verbose:     Cfg.getParamBool('misc.verboseLevel1').setBool(True)
  if options.veryVerbose: Cfg.getParamBool('misc.verboseLevel2').setBool(True)

  print framework.getEnvironment().getPrint()

  success = ScriptMain( **kw )
  shellSuccess = 0
  if not success: shellSuccess = 1

  sys.exit( shellSuccess )
